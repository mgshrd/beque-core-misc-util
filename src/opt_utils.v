Definition option_flat {A} (oo : option (option A)) : option A :=
  match oo with
    | Some (Some v) => Some v
    | _ => None
  end.

Definition option_bind {A B} (o : option A) (f : A -> option B) : option B :=
  option_flat (option_map f o).

Definition optpred {A} (P : A -> Prop) (oa : option A) :=
  match oa with
    | Some a => P a
    | None => False
  end.

Require Import List.
Require Import Omega.

Require Import sublist.
Require Import sublist_facts.
Require Import list_eq_part.

Record Segmenter
  := { segmenter_A : Type
       ; segmenter_P : list segmenter_A -> Prop
       ; segmenter_validP : forall s1 s2,
                              segmenter_P s1 ->
                              segmenter_P s2 ->
                              proper_sublist_of s1 s2 ->
                              False
       ; segmenter_validPnotnil : forall s,
                                    segmenter_P s ->
                                    s <> nil
     }.

(** The list consists of segments. *)
Inductive fully_segmented
          (S : Segmenter) :
  list (segmenter_A S) -> Prop :=
| fs_nil : fully_segmented S nil
| fs_app : forall s t,
             (segmenter_P S) s ->
             fully_segmented S t ->
             fully_segmented S (s ++ t).

Lemma fully_segmented_inv :
  forall S (s l : list (segmenter_A S)),
    fully_segmented S (s ++ l) ->
    (segmenter_P S) s ->
    fully_segmented S l.
Proof.
  intros.
  inversion H.
  - destruct s; [ | discriminate H2 ].
    destruct l; [ | discriminate H2 ].
    exact H.
  - clear H.
    destruct (Compare_dec.lt_eq_lt_dec (length s0) (length s))
      as [ [ s0shorter | s0seq ] | sshorter ];
      [
        idtac
      | destruct (app_eq_len _ _ _ _ s0seq H1); subst s0; subst t; exact H3
      | idtac
      ].
    + assert (length s0 <= length s); [ omega | ].
      assert (Hz := expose_prefix _ _ _ _ _ H1 H). clear H.
      destruct Hz. subst s. rewrite <- app_assoc in H1.
      assert (Hy := app_inv_head _ _ _ H1). subst t. clear H1.
      destruct x; [ exact H3 | ].
      assert (Hx := segmenter_validP S _ _ H2 H0).
      assert (Hy := segmenter_validPnotnil S _ H2).
      elim Hx. exists nil; exists (s::x).
      split; [ exact Hy | ].
      split; [ | reflexivity ].
      intro. exact (list_eq_append _ _ _ H).

    + assert (Hx := segmenter_validP S _ _ H0 H2).
      assert (Hy := segmenter_validPnotnil S _ H0).
      assert (prefix_of s0 (s ++ l)).
      { rewrite <- H1. exact (ex_intro _ t eq_refl). }
      assert (Hz := shorter_prefix_proper_sublist_of _ _ (s ++ l) Hy (ex_intro _ l eq_refl) H sshorter).
      contradiction (Hx Hz).
Qed.

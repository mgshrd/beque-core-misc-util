Require Import sigS.

(** Generate a function taking the last fragment from a value of type [T]. Where [T] is a (possibly deeply embedded) { _ : _ & { _ : _ & ... } } structure *)
Ltac takelastT T :=
  match T with
    (* prefer depending part *)
    | sigS (fun _ : _ => ?U) =>
      let tmp := takelastT U in
      constr:(fun z : T => tmp (projT2 z))
    | {_ : _ & ?U } =>
      let tmp := takelastT U in
      constr:(fun z : T => tmp (projT2 z))

    | ?t => constr:(fun z : t => z)
  end.
Ltac takelast v :=
  match type of v with
  | ?T => takelastT T
  end.

(** Generate a function taking the deepest fragment matching type [t] from a value of type [T]. Where [T] is a recursive { _ : _ & { _ : _ & ... } } structure *)
Ltac gen_accessor T t :=
  match T with
    (* direct hit *)
    | t => constr:(fun z : t => z)

    (* direct mappings of [t] *)
    | sigS (fun _ : ?X => t) =>
      constr:(fun z : T => projT2 z)
    | {_ : ?X & t } =>
      constr:(fun z : T => projT2 z)
    | sigS (fun _ : t => ?U) =>
      constr:(fun z : T => projT1 z)
    | {_ : t & ?U } =>
      constr:(fun z : T => projT1 z)
    | prod t ?B =>
      constr:(fun z : T => fst z)
    | prod ?A t =>
      constr:(fun z : T => snd z)

    (* prefer depending part *)
    | sigS (fun _ : ?X => ?U) =>
      let tmp := gen_accessor U t in
      constr:(fun z : T => tmp (projT2 z))
    | {_ : ?X & ?U } =>
      let tmp := gen_accessor U t in
      constr:(fun z : T => tmp (projT2 z))
    (* visit the non-dependent(?) part here *)
    | sigS (fun _ : ?X => ?U) =>
      let tmp := gen_accessor X t in
      constr:(fun z : T => tmp (projT1 z))
    | {_ : ?X & ?U } =>
      let tmp := gen_accessor X t in
      constr:(fun z : T => tmp (projT1 z))
    | prod ?A ?B =>
      let tmp := gen_accessor A t in
      constr:(fun z : T => tmp (fst z))
    | prod ?A ?B =>
      let tmp := gen_accessor B t in
      constr:(fun z : T => tmp (snd z))
  end.
Ltac gen_accessor_for v gt :=
  match type of v with
  | ?vt => gen_accessor vt gt
  end.
(** Generate a function, that looks through [v] for a value matching the goal's type. Prefers dependent embedded values. *)
Ltac gen_accessor_for_goal v :=
  match goal with
    |- ?gt => gen_accessor_for v gt
  end.
(** Get a value from [v] matching the goal. Prefers deeply embedded values. *)
Ltac takematching v :=
  let accessor := gen_accessor_for_goal v in
  let tmp := fresh in
  set (tmp := accessor); simpl in tmp;
  exact (accessor v).

Set Warnings "-non-reversible-notation".
(* find a part of [v] that has type [T] *)
Notation "'focusto' T 'inside' v" := (ltac:(takematching v):T) (at level 10).
(* take the last depending value of the [{_:_ & {_:_ & {...}}}] typed value [y] *)
Notation "'focuslast' y" := (ltac:(let x := takelast ((fun x => x) y (* TODO specifying [y] here did not work *)) in exact (x y))) (at level 10).

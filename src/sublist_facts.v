Require Import List.
Require Import Arith.
Require Import Omega.

Require Import rev_eq_eq.
Require Import list_eq_part.
Require Import ltac_utils.

Require Import sublist.

Lemma prefix_rev_postfix0 :
  forall {A} (p l : list A),
    prefix_of p l <-> postfix_of (rev p) (rev l).
Proof.
  intros. split; intro.
  - destruct p.
    { intros. exists (rev l). rewrite app_nil_r. reflexivity. }
    intros. destruct l.
    { destruct H. discriminate H. }
    destruct H. injit H.
    simpl. rewrite rev_app_distr. rewrite <- app_assoc. exists (rev x). reflexivity.
  - destruct p.
    { intros. exists l. reflexivity. }
    destruct H. simpl in H.
    assert (x ++ rev p ++ a::nil = rev ((a::p) ++ rev x)).
    { simpl. rewrite rev_app_distr. rewrite <- app_assoc. rewrite rev_involutive. reflexivity. }
    rewrite H0 in H.
    assert (Hx := rev_eq_eq _ _ H). subst l.
    exists (rev x). reflexivity.
Qed.

Lemma prefix_rev_postfix1 :
  forall {A} (p l : list A),
    prefix_of p (rev l) <-> postfix_of (rev p) l.
Proof.
  intros.
  assert (Hx := prefix_rev_postfix0 p (rev l)).
  rewrite rev_involutive in Hx. exact Hx.
Qed.

Lemma prefix_rev_postfix2 :
  forall {A} (p l : list A),
    prefix_of (rev p) l <-> postfix_of p (rev l).
Proof.
  intros.
  assert (Hx := prefix_rev_postfix0 (rev p) l).
  rewrite rev_involutive in Hx. exact Hx.
Qed.

Lemma prefix_rev_postfix3 :
  forall {A} (p l : list A),
    prefix_of (rev p) (rev l) <-> postfix_of p l.
Proof.
  intros.
  assert (Hx := prefix_rev_postfix0 (rev p) (rev l)).
  repeat rewrite rev_involutive in Hx. exact Hx.
Qed.

Lemma postfix_of_em
      {A}
      (eq_A_em : forall a1 a2 : A, a1=a2 \/ a1<>a2)
      (p l : list A) :
  postfix_of p l \/ ~postfix_of p l.
Proof.
  revert l. induction p.
  { intros. left. exists l. rewrite app_nil_r. reflexivity. }
  intro.
  destruct (IHp l).
  - destruct H. subst l.
    induction x using rev_ind; [ | clear IHx ].
    + right. intro. destruct H.
      assert ((x ++ a::nil) ++ p = x ++ a::p).
      { rewrite <- app_assoc. simpl. reflexivity. }
      rewrite <- H0 in H. assert (Hx := app_inv_tail _ _ _ H).
      destruct x; discriminate Hx.
    + rewrite <- app_assoc. simpl.
      destruct (eq_A_em a x).
      * subst x. left. exists x0. reflexivity.
      * right. intro. destruct H0.
        assert (x1 ++ a :: p = (x1 ++ a::nil) ++ p).
        { rewrite <- app_assoc. reflexivity. }
        rewrite H1 in H0.
        assert (x0 ++ x :: p = (x0 ++ x::nil) ++ p).
        { rewrite <- app_assoc. reflexivity. }
        rewrite H2 in H0.
        assert (Hx := app_inv_tail _ _ _ H0).
        assert (Hy := app_inj_tail _ _ _ _ Hx).
        destruct Hy. elim H. exact H4.
  - right. intro. elim H. destruct H0.
    subst l. exists (x ++ a::nil). rewrite <- app_assoc. simpl. reflexivity.
Qed.

Lemma proper_postfix_of_em
      {A}
      (eq_A_em : forall a1 a2 : A, a1=a2 \/ a1<>a2)
      (p l : list A) :
  proper_postfix_of p l \/ ~proper_postfix_of p l.
Proof.
  destruct (postfix_of_em eq_A_em p l).
  - destruct (eq_nat_dec (length p) (length l)).
    + right. intro. destruct H. subst l.
      destruct x.
      * destruct H0. destruct H. destruct H0.
        assert (Hx := app_inv_tail _ _ _ H1).
        elim H0. reflexivity.
      * rewrite app_length in e. simpl in e. omega.
    + destruct p; [ right | left ].
      { intro. destruct H0. elim (proj1 H0). reflexivity. }
      destruct H. subst l.
      destruct x.
      * elim n. reflexivity.
      * exists (a0::x).
        split; [ intro d; discriminate d | ].
        split; [ | reflexivity ].
        intro. simpl in H. exact (list_eq_prepend _ _ _ H).
  - right. intro. elim H.
    destruct H0. destruct H0. destruct H1. subst l. exists x. reflexivity.
Qed.

Lemma prefix_of_em
      {A}
      (eq_A_em : forall a1 a2 : A, a1=a2 \/ a1<>a2)
      (p l : list A) :
  prefix_of p l \/ ~prefix_of p l.
Proof.
  rewrite prefix_rev_postfix0.
  exact (postfix_of_em eq_A_em _ _).
Qed.

Lemma proper_prefix_of_em
      {A}
      (eq_A_em : forall a1 a2 : A, a1=a2 \/ a1<>a2)
      (p l : list A) :
  proper_prefix_of p l \/ ~proper_prefix_of p l.
Proof.
  destruct (prefix_of_em eq_A_em p l); [ | right ].
  - destruct (eq_nat_dec (length p) (length l)); [ right | ].
    + intro. destruct H0. destruct H0. destruct H1. subst l.
      rewrite app_length in e.
      destruct x; [ elim H1; rewrite app_nil_r; reflexivity | ].
      simpl in e. omega.
    + destruct H. subst l. destruct p; [ right | left ].
      * intro. destruct H. exact ((proj1 H) eq_refl).
      * exists x.
        split; [ | split; [ | reflexivity ] ].
        { intro d; discriminate d. }
        intro. destruct x.
        { rewrite app_nil_r in n. elim n. reflexivity. }
        exact (list_eq_append _ _ _ H).
  - intro. elim H. clear H.
    destruct H0. destruct H. destruct H0. subst l.
    exists x. reflexivity.
Qed.

Lemma sublist_post_pre :
  forall A (s l : list A),
    sublist_of s l <-> exists s', postfix_of s' l /\ prefix_of s s'.
Proof.
  intros; split; intros.
  - unfold sublist_of in H. destruct H as [ pre [ post ppf ] ]. subst l.
    exists (s ++ post). split.
    + exists pre. reflexivity.
    + exists post. reflexivity.
  - unfold sublist_of.
    destruct H as [ s' H ].
    destruct H as [ postpf prepf ].
    destruct postpf as [ pre lppf ]. subst l.
    destruct prepf as [ post s'ppf ]. subst s'.
    exists pre. exists post. reflexivity.
Qed.

Lemma sublist_of_em
      {A}
      (eq_A_em : forall a1 a2 : A, a1=a2 \/ a1<>a2)
      (s l : list A) :
  sublist_of s l \/ ~sublist_of s l.
Proof.
  revert s. induction l.
  - intros.
    destruct s.
    + left. exists nil. exists nil. reflexivity.
    + right. intro. destruct H. destruct H. destruct x; discriminate H.
  - intros.
    destruct (prefix_of_em eq_A_em s (a::l)); [ left | ].
    + destruct H. exists nil. exists x. rewrite H. reflexivity.
    + destruct (IHl s).
      { left. destruct H0. destruct H0. exists (a::x). exists x0. subst l. simpl. reflexivity. }
      right. intro. destruct H1. destruct H1. destruct x; [ destruct s; [ destruct x0; [ discriminate H1 | ] | ] | ]; simpl in H1; injit H1.
      * elim H0. exists nil. exists l. reflexivity.
      * elim H. exists x0. reflexivity.
      * elim H0. exists x. exists x0. reflexivity.
Qed.

Lemma proper_sublist_of_em
      {A}
      (eq_A_em : forall a1 a2 : A, a1=a2 \/ a1<>a2)
      (s l : list A) :
  proper_sublist_of s l \/ ~proper_sublist_of s l.
Proof.
  destruct (sublist_of_em eq_A_em s l).
  - destruct (eq_nat_dec (length s) (length l)).
    + right. intro. destruct H0. destruct H0. destruct H0. destruct H1.
      subst l.
      repeat rewrite app_length in e.
      destruct x; [ | simpl in e; omega ].
      destruct x0; [ | simpl in e; omega ].
      elim H1.
      simpl. rewrite app_nil_r. reflexivity.
    + destruct s; [ right | left ].
      { intro. destruct H0. destruct H0.  exact ((proj1 H0) eq_refl). }
      destruct H. destruct H. exists x. exists x0.
      split; [ intro d; discriminate d | ].
      split; [ | exact H ].
      intro. elim n. rewrite H0. reflexivity.
  - right. intro. elim H. clear H.
    destruct H0. destruct H. destruct H. destruct H0.
    exists x. exists x0. exact H1.
Qed.

Lemma sublist_pre_post :
  forall A (s l : list A),
    sublist_of s l <-> exists s', prefix_of s' l /\ postfix_of s s'.
Proof.
  intros; split; intros.
  - unfold sublist_of in H. destruct H as [ pre [ post ppf ] ]. subst l.
    exists (pre ++ s). split.
    + exists post. rewrite app_assoc. reflexivity.
    + exists pre. reflexivity.
  - unfold sublist_of.
    destruct H as [ s' H ].
    destruct H as [ prepf postpf ].
    destruct prepf as [ post lppf ]. subst l.
    destruct postpf as [ pre s'ppf ]. subst s'.
    exists pre. exists post. rewrite app_assoc. reflexivity.
Qed.

Lemma prefix_of_app :
  forall {A} (l ll p : list A),
    prefix_of p l -> prefix_of p (l ++ ll).
Proof.
  induction l.
  - intros. destruct p.
    + exists ll. reflexivity.
    + exfalso. destruct H. discriminate H.
  - intros.
    destruct p.
    { exists ((a::l)++ll). reflexivity. }
    destruct H. injit H.
    exists (x ++ ll). simpl. rewrite <- app_assoc. reflexivity.
Qed.

Lemma postfix_of_app :
  forall {A} (ll l p : list A),
    postfix_of p ll -> postfix_of p (l ++ ll).
Proof.
  induction ll.
  - intros. destruct p.
    + exists l. reflexivity.
    + exfalso. destruct H. destruct x; discriminate H.
  - intros.
    destruct p.
    { exists (l++a::ll). rewrite app_nil_r. reflexivity. }
    destruct H. destruct x.
    + injit H. exists l. reflexivity.
    + simpl in H. injit H. exists (l ++ a::x). rewrite <- app_assoc. simpl. reflexivity.
Qed.

Lemma postfix_of_nil :
  forall {A} (l : list A),
    postfix_of l nil -> l = nil.
Proof.
  intros. destruct l; [ reflexivity | ].
  destruct H. destruct x; discriminate H.
Qed.

Lemma prefix_of_nil :
  forall {A} (l : list A),
    prefix_of l nil -> l = nil.
Proof.
  intros. destruct l; [ reflexivity | ].
  destruct H. discriminate H.
Qed.

Lemma expose_prefix :
  forall A (h1 t1 h2 t2 : list A),
    h1 ++ t1 = h2 ++ t2 ->
    length h1 <= length h2 ->
    exists h, h2 = h1 ++ h.
Proof.
  intros.
  revert dependent h2.
  induction h1.
  - intros. simpl in *. subst. exists h2. reflexivity.
  - intros.
    destruct h2; [ simpl in *; omega | ].
    injit H.
    assert (length h1 <= length h2).
    { simpl in *. omega. }
    assert (IHh1' := IHh1 _ H H1). clear IHh1.
    destruct IHh1'. subst h2.
    exists x.
    reflexivity.
Qed.

Lemma expose_cons_prefix :
  forall A l ll (a : A) t,
    l ++ ll = a::t ->
    t = skipn 1 (l ++ ll).
Proof.
  intros. destruct l, ll; [ discriminate H | | | ].
  - injit H. simpl. reflexivity.
  - rewrite app_nil_r in H.
    injit H. simpl. rewrite app_nil_r. reflexivity.
  - injit H. simpl. reflexivity.
Qed.

Lemma expose_postfix :
  forall A (h1 t1 h2 t2 : list A),
    h1 ++ t1 = h2 ++ t2 ->
    length t1 <= length t2 ->
    exists h, t2 = h ++ t1.
Proof.
  intros.
  revert dependent t2.
  induction t1 using rev_ind.
  - intros. exists t2. rewrite app_nil_r. reflexivity.
  - intros.
    induction t2 as [ | ? ? _ ] using rev_ind.
    { rewrite app_length in H0. simpl in *. omega. }
    assert (length t1 <= length t2).
    { repeat rewrite app_length in H0. simpl in *. omega. }
    repeat rewrite app_assoc in H.
    destruct (app_inj_tail _ _ _ _ H). subst x0.
    assert (IHt1' := IHt1 _ H2 H1). clear IHt1.
    destruct IHt1'. subst t2.
    exists x0.
    rewrite <- app_assoc. reflexivity.
Qed.

Lemma prefix_of_app_elim :
  forall {A} (ll l p : list A),
    prefix_of p (l ++ ll) -> prefix_of p l \/ (exists p', prefix_of p' ll /\ l ++ p' = p).
Proof.
  intros. destruct H.
  destruct (le_lt_dec (length p) (length l)); [ left | right ].
  - assert (Hx := expose_prefix _ _ _ _ _ H l0).
    destruct Hx. subst l.
    exists x0. reflexivity.
  - assert (length x <= length ll).
    {
      assert (length (p ++ x) = length (l ++ ll)); [ rewrite H; reflexivity | ].
      repeat rewrite app_length in H0. omega.
    }
    assert (Hx := expose_postfix _ _ _ _ _ H H0).
    destruct Hx. subst ll.
    exists x0. split; [ exists x; reflexivity | ].
    rewrite app_assoc in H.
    assert (Hx := app_inv_tail _ _ _ H). rewrite Hx. reflexivity.
Qed.

Lemma postfix_of_app_elim :
  forall {A} (ll l p : list A),
    postfix_of p (l ++ ll) -> postfix_of p ll \/ (exists p', postfix_of p' l /\ p' ++ ll = p).
Proof.
  intros. destruct H.
  destruct (le_lt_dec (length p) (length ll)); [ left | right ].
  - assert (Hx := expose_postfix _ _ _ _ _ H l0).
    destruct Hx. subst ll.
    exists x0. reflexivity.
  - assert (length x <= length l).
    {
      assert (length (x ++ p) = length (l ++ ll)); [ rewrite H; reflexivity | ].
      repeat rewrite app_length in H0. omega.
    }
    assert (Hx := expose_prefix _ _ _ _ _ H H0).
    destruct Hx. subst l.
    exists x0. split; [ exists x; reflexivity | ].
    rewrite <- app_assoc in H.
    assert (Hx := app_inv_head _ _ _ H). rewrite Hx. reflexivity.
Qed.

Lemma sublist_of_trans :
  forall {A} (l sl ssl : list A),
    sublist_of sl l ->
    sublist_of ssl sl ->
    sublist_of ssl l.
Proof.
  intros.
  destruct H as [ spre [ spost ldef ] ]. subst l.
  destruct H0 as [ sspre [ sspost sldef ] ]. subst sl.
  exists (spre ++ sspre). exists (sspost ++ spost).
  repeat rewrite <- app_assoc. reflexivity.
Qed.

Lemma sublist_of_app_l :
  forall {A} (l ll sl : list A),
    sublist_of sl l ->
    sublist_of sl (l ++ ll).
Proof.
  intros.
  apply (sublist_of_trans (l ++ ll) l sl).
  - exists nil. exists ll. reflexivity.
  - exact H.
Qed.

Lemma sublist_of_app_r :
  forall {A} (l ll sl : list A),
    sublist_of sl ll ->
    sublist_of sl (l ++ ll).
Proof.
  intros.
  apply (sublist_of_trans (l ++ ll) ll sl).
  - exists l. exists nil. rewrite app_nil_r. reflexivity.
  - exact H.
Qed.

Lemma forall_postfix_of_app :
  forall {A} (l ll : list A) P,
    (forall p, postfix_of p (l ++ ll) -> P p) -> (forall p, postfix_of p ll -> P p).
Proof.
  destruct ll.
  - intros. apply X.
    unfold postfix_of in *. exists l.
    apply f_equal2; [ reflexivity | ].
    destruct H. exact (proj2 (app_eq_nil _ _ H)).
  - intros.
    assert (X' := X p). clear X.
    apply X'.
    destruct H. exists (l ++ x).
    rewrite <- app_assoc. rewrite H. reflexivity.
Qed.

Lemma forall_prefix_of_app :
  forall {A} (l ll : list A) P,
    (forall p, prefix_of p (l ++ ll) -> P p) -> (forall p, prefix_of p l -> P p).
Proof.
  destruct l.
  - intros. apply X.
    unfold prefix_of in *. exists ll.
    apply f_equal2; [ | reflexivity ].
    destruct H. exact (proj1 (app_eq_nil _ _ H)).
  - intros.
    assert (X' := X p). clear X.
    apply X'.
    destruct H. exists (x ++ ll).
    rewrite app_assoc. rewrite H. reflexivity.
Qed.

Lemma forall_sublist_app :
  forall {A} (l ll : list A) (P : list A -> Prop),
    (forall sl, sublist_of sl (l ++ ll) -> P sl) ->
    ((forall sl, sublist_of sl l -> P sl) /\
     (forall sl, sublist_of sl ll -> P sl)).
Proof.
  intros. split.
  - destruct l.
    + intros.
      destruct H0. destruct H0. destruct (app_eq_nil _ _ H0). destruct (app_eq_nil _ _ H2). subst.
      apply H. exists nil. exists ll. reflexivity.
    + intros.
      apply H.
      apply sublist_of_app_l.
      exact H0.
  - intros.
    apply H.
    apply sublist_of_app_r.
    exact H0.
Qed.

Lemma sublist_prefix :
  forall {A} (l : list A) (P : list A -> Prop),
    (forall sl, sublist_of sl l -> P sl) -> (forall p, prefix_of p l -> P p).
Proof.
  intros. destruct H0 as [ post ldef ].
  specialize H with p. apply H.
  exists nil. exists post. rewrite ldef. reflexivity.
Qed.

Lemma sublist_postfix :
  forall {A} (l : list A) (P : list A -> Prop),
    (forall sl, sublist_of sl l -> P sl) -> (forall p, postfix_of p l -> P p).
Proof.
  intros. destruct H0 as [ pre ldef ].
  specialize H with p. apply H.
  exists pre. exists nil. rewrite app_assoc. rewrite ldef. rewrite app_nil_r. reflexivity.
Qed.

Lemma postfix_is_sublist :
  forall {A} (s l : list A),
    postfix_of s l -> sublist_of s l.
Proof.
  intros. destruct H. subst l.
  exists x. exists nil. rewrite app_nil_r. reflexivity.
Qed.

Lemma prefix_is_sublist :
  forall {A} (s l : list A),
    prefix_of s l -> sublist_of s l.
Proof.
  intros. destruct H. subst l.
  exists nil. exists x. reflexivity.
Qed.

Lemma not_proper_but_sublist_eq :
  forall {A} (s l : list A) (snnil : s <> nil),
    sublist_of s l ->
    ~proper_sublist_of s l ->
    s = l.
Proof.
  intros. revert dependent s. induction l.
  - intros. destruct H. destruct H.
    destruct x; [ | discriminate H ].
    destruct s; [ reflexivity | discriminate H ].
  - intros.
    destruct H. destruct H.
    destruct x; [ destruct s; destruct x0 | ].
    + discriminate H.
    + elim snnil. reflexivity.
    + rewrite <- H. rewrite app_nil_r. reflexivity.
    + simpl in H. injit H.
      elim H0.
      clear. exists nil. exists (a1::x0).
      split; [ intro d; discriminate d | ].
      split.
      * intro d. injection d; clear d; intros. exact (list_eq_append _ _ _ H).
      * reflexivity.
    + injit H.
      elim H0.
      exists (a::x). exists x0.
      split; [ exact snnil | ].
      split.
      * clear. intro d.
        exact (list_eq_cons_mid _ _ _ _ d).
      * reflexivity.
Qed.

Lemma sublist_of_nil :
  forall {A} (l : list A),
    sublist_of l nil -> l = nil.
Proof.
  intros. destruct H. destruct H.
  destruct x; [ | discriminate H ].
  destruct l; [ | discriminate H ].
  reflexivity.
Qed.

Lemma postfix_of_cons_nil :
  forall {A} (a a' : A) l,
    postfix_of (a::l) (a'::nil) -> l = nil /\ a = a'.
Proof.
  intros. destruct H.
  destruct x; [ | destruct x ]; try discriminate H.
  destruct l; [ | discriminate H ].
  injit H. split; reflexivity.
Qed.

Lemma prefix_of_cons_nil :
  forall {A} (a a' : A) l,
    prefix_of (a::l) (a'::nil) -> l = nil /\ a = a'.
Proof.
  intros. destruct H.
  destruct l; [ | discriminate H ].
  destruct x; [ | discriminate H ].
  rewrite app_nil_r in H. injit H. split; reflexivity.
Qed.

Lemma sublist_of_cons_nil :
  forall {A} (a a' : A) l,
    sublist_of (a::l) (a'::nil) -> l = nil /\ a = a'.
Proof.
  intros. destruct H. destruct H.
  destruct x; [ | destruct x ]; try discriminate H.
  destruct l; [ | discriminate H ].
  destruct x0; [ | discriminate H ].
  rewrite app_nil_r in H. injit H. split; reflexivity.
Qed.

Lemma app_proper_split :
  forall {A} (s1 s2 l : list A),
    s1 ++ s2 = l ->
    s1 <> nil ->
    s2 <> nil ->
    (proper_sublist_of s1 l /\ proper_sublist_of s2 l).
Proof.
  intros.
  split.
  - unfold proper_sublist_of. exists nil. exists s2.
    split; [ exact H0 | ].
    split; [ | exact H ].
    intro. subst s1.
    assert (l = l ++ nil).
    { rewrite app_nil_r. reflexivity. }
    rewrite H2 in H at 2.
    assert (Hx := app_inv_head _ _ _ H). subst s2. elim H1. reflexivity.
  - unfold proper_sublist_of. exists s1. exists nil.
    split; [ exact H1 | ].
    split; [ | rewrite app_nil_r; exact H ].
    intro. subst s2.
    assert (l = nil ++ l); [ reflexivity | ].
    rewrite H2 in H at 2.
    assert (Hx := app_inv_tail _ _ _ H). subst s1. elim H0. reflexivity.
Qed.

Lemma shorter_prefix_proper_sublist_of :
  forall {A} (p1 p2 l : list A) (p1nnil : p1 <> nil),
    prefix_of p1 l ->
    prefix_of p2 l ->
    length p1 < length p2 ->
    proper_sublist_of p1 p2.
Proof.
  intros.
  destruct H. destruct H0. rewrite <- H0 in H.
  assert (length p1 <= length p2); [ omega | ].
  assert (Hx := expose_prefix _ _ _ _ _ H H2). clear H2.
  destruct Hx. subst p2.
  destruct x1.
  { rewrite app_length in H1. simpl in H1. omega. }
  unfold proper_sublist_of. exists nil. exists (a::x1).
  split.
  * intro. subst p1. elim p1nnil. reflexivity.
  * split; [ | reflexivity ].
    intro. exact (list_eq_append _ _ _ H2).
Qed.

Lemma app_eq_len_head :
  forall {A} (h1 t1 h2 t2 : list A),
    length h1 = length h2 ->
    h1 ++ t1 = h2 ++ t2 ->
    h1 = h2.
Proof.
  induction h1.
  { intros. destruct h2; [ | discriminate H ]. reflexivity. }
  intros. destruct h2; [ discriminate H | ].
  simpl in H0. injit H0.
  apply f_equal2; [ reflexivity | ].
  refine (IHh1 _ _ _ _ H0).
  simpl in H. injection H. intros. exact H1.
Qed.

Lemma app_eq_len :
  forall {A} (h1 t1 h2 t2 : list A),
    length h1 = length h2 ->
    h1 ++ t1 = h2 ++ t2 ->
    (h1 = h2 /\ t1 = t2).
Proof.
  intros. assert (Hx := app_eq_len_head _ _ _ _ H H0).
  split; [ exact Hx | ].
  subst h2.
  apply app_inv_head with h1.
  exact H0.
Qed.

Lemma expose_common_prefix :
  forall A (h1 t1 h2 t2 : list A),
    h1 ++ t1 = h2 ++ t2 ->
    exists h h1t h2t, h ++ h1t = h1 /\ h ++ h2t = h2.
Proof.
  induction h1.
  - intros. simpl in *. subst t1. exists nil. exists nil. exists h2. auto.
  - simpl. intros.
    destruct h2; [ destruct t2; [ discriminate H | ] | ].
    + injit H.
      assert (IHh1' := IHh1 t1 h1 t1 eq_refl). clear IHh1.
      destruct IHh1' as [ h [ h1t [ h2t [ h1def h2nil ] ] ] ].
      subst h1. assert (Hx := app_inv_head _ _ _ h2nil). subst.
      exists nil. exists (a0::h++h1t). exists nil. auto.
    + injit H.
      assert (IHh1' := IHh1 _ _ _ H). clear IHh1.
      destruct IHh1' as [ h [ h1t [ h2t [ h1def h2def ] ] ] ].
      subst h1. subst h2.
      exists (a0::h). exists h1t. exists h2t.
      split; [ reflexivity | reflexivity ].
Qed.

Lemma forall_postfix_of_join :
  forall {A} (P : list A -> Prop) s2 s1 l,
    (forall p, postfix_of p s1 -> P (p ++ l)) ->
    (forall p, postfix_of p s2 -> P (p ++ s1 ++ l)) ->
    (forall p, postfix_of p (s2 ++ s1) -> P (p ++ l)).
Proof.
  intros. unfold prefix_of in *. destruct H1 as [ t' H1 ].
  destruct (le_lt_dec (length t') (length s2)) as [ t'shorter | s2shorter ].
  - assert (Hx := expose_prefix _ _ _ _ _ H1 t'shorter).
    destruct Hx. subst s2. rewrite <- app_assoc in H1.
    assert (Hx := app_inv_head _ _ _ H1). subst p. clear H1.
    rewrite <- app_assoc.
    apply H0. exists t'. reflexivity.
  - assert (s2 ++ s1 = t' ++ p); [ rewrite H1; reflexivity | ].
    assert (length s2 <= length t'); [ omega | ]. clear s2shorter. rename H3 into s2shorter.
    assert (Hx := expose_prefix _ _ _ _ _ H2 s2shorter).
    destruct Hx. subst t'. rewrite <- app_assoc in H2.
    assert (Hx := app_inv_head _ _ _ H2). subst s1. clear H1 H2.
    apply H. exists x. reflexivity.
Qed.

Lemma proper_prefix_or_nil_or_eq :
  forall {A} (p l : list A),
    prefix_of p l ->
    (p = nil \/ p = l \/ proper_prefix_of p l).
Proof.
  intros. destruct H. subst.
  destruct p; [ left; reflexivity | right ].
  destruct x; [ left | right ].
  - rewrite app_nil_r. reflexivity.
  - exists (a0::x). split; [ | split ].
    + intro d; discriminate d.
    + intro. exact (list_eq_append _ _ _ H).
    + reflexivity.
Qed.

Lemma proper_postfix_or_eq :
  forall {A} (p l : list A),
    postfix_of p l ->
    (p = nil \/ p = l \/ proper_postfix_of p l).
Proof.
  intros. destruct H. subst.
  destruct p; [ left; reflexivity | right ].
  destruct x; [ left | right ].
  - reflexivity.
  - exists (a0::x). split; [ | split ].
    + intro d; discriminate d.
    + intro. exact (list_eq_prepend _ _ _ H).
    + reflexivity.
Qed.

Lemma proper_sublist_or_eq :
  forall {A} (p l : list A),
    sublist_of p l ->
    (p = nil \/ p = l \/ proper_sublist_of p l).
Proof.
  intros. destruct H. destruct H. subst.
  destruct p; [ left; reflexivity | right ].
  destruct x; [ destruct x0; [ left | right ] | right ].
  - rewrite app_nil_r. reflexivity.
  - exists nil. exists (a0::x0). split; [ | split ].
    + intro. discriminate H.
    + intro. exact (list_eq_mid_cons _ _ _ _ H).
    + reflexivity.
  - exists (a0::x). exists x0. split; [ | split ].
    + intro d; discriminate d.
    + intro. exact (list_eq_cons_mid _ _ _ _ H).
    + reflexivity.
Qed.

Lemma postfix_of_cons :
  forall {A} p (a : A) l,
    postfix_of p (a::l) ->
    (postfix_of p l \/ p = a::l).
Proof.
  intros. destruct H. destruct x; [ right | left ].
  - rewrite <- H. reflexivity.
  - exists x. injit H. reflexivity.
Qed.

Lemma prefix_of_rcons :
  forall {A} p (a : A) l,
    prefix_of p (l ++ a::nil) ->
    (prefix_of p l \/ p = l ++ a::nil).
Proof.
  intros. destruct H. induction x using rev_ind; [ right | left; clear IHx ].
  - rewrite <- H. rewrite app_nil_r. reflexivity.
  - exists x0.
    rewrite app_assoc in H.
    assert (Hx := app_inj_tail _ _ _ _ H).
    exact (proj1 Hx).
Qed.

Lemma prefix_of_app_elim3way :
  forall {A} {ll x l p : list A},
    (p ++ x = l ++ ll) ->
    (exists p', p' <> nil /\ p ++ p' = l /\ x = p' ++ ll) \/
    (p = l /\ x = ll) \/
    (exists p', p' <> nil /\ p = l ++ p' /\ p' ++ x = ll).
Proof.
  intros.
  assert (prefix_of p (l ++ ll)); [ exists x; assumption | ].
  destruct (prefix_of_app_elim _ _ _ H0).
  - destruct H1. subst l. destruct x0; [ | destruct p ].
    + right; left.
      rewrite app_nil_r in *. split; [ reflexivity | ].
      assert (Hx := app_inv_head _ _ _ H). assumption.
    + left.
      simpl in H. subst x. exists (a::x0).
      split; [ | split; reflexivity ].
      intro d; discriminate d.
    + left.
      rewrite <- app_assoc in H.
      assert (Hx := app_inv_head _ _ _ H). subst x.
      exists (a::x0). split; [ | split ].
      * intro d; discriminate d.
      * reflexivity.
      * reflexivity.
  - destruct H1. destruct H1. subst p.
    rewrite <- app_assoc in H. assert (Hx := app_inv_head _ _ _ H). subst ll. clear H.
    clear H1. clear H0. destruct x0.
    + right; left.
      rewrite app_nil_r. split; reflexivity.
    + right; right.
      exists (a::x0). split; [ | split ].
      * intro d; discriminate d.
      * reflexivity.
      * reflexivity.
Qed.

Lemma prefix_of_app_elim5way :
  forall {A} {ll x l p : list A},
    (p ++ x = l ++ ll) ->
    (p = nil /\ x = l ++ ll) \/
    (exists p', p <> nil /\ p' <> nil /\ p ++ p' = l /\ x = p' ++ ll) \/
    (p = l /\ x = ll) \/
    (exists p', x <> nil /\ p' <> nil /\ p = l ++ p' /\ p' ++ x = ll) \/
    (x = nil /\ p = l ++ ll).
Proof.
  intros.
  destruct p; [ left; split; [ reflexivity | ]; simpl in H; exact H | right ].
  destruct x; [ right; right; right; split; [ reflexivity | rewrite app_nil_r in H; exact H ] | ].
  assert (Hx := prefix_of_app_elim3way H).
  destruct Hx as [ ? | [ ? | ? ] ]; firstorder.
Qed.



Lemma postfix_of_refl :
  forall {A} (l : list A),
    postfix_of l l.
Proof.
  intros. exists nil. reflexivity.
Qed.



Lemma prefix_of_refl :
  forall {A} (l : list A),
    prefix_of l l.
Proof.
  intros. exists nil. rewrite app_nil_r. reflexivity.
Qed.

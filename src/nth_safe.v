Fixpoint nth_safe_pf
           {A} (l : list A)
           {x} (n : x < length l) :
  A.
Proof.
  refine (match l as lx, x as xx return l = lx /\ x = xx -> A with
          | cons h _, O => fun _ => h
          | cons _ t, S x' => fun eqpf => nth_safe_pf A t x' _
          | nil, _ => fun eqpf => False_rect _ _ A
          end (conj eq_refl eq_refl)); [ idtac | idtac ].
  - destruct eqpf. subst l.
    simpl in n. exfalso. inversion n.
  - destruct eqpf. subst l. subst x.
    simpl in n.
    unfold lt in *. assert (Hx := le_S_n _ _ n). exact Hx.
Defined.
Definition nth_safe_sig {A} (l : list A) (n : {n | n < length l}) := nth_safe_pf l (proj2_sig n).

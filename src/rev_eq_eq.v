Require Import List.

Lemma rev_eq_eq :
  forall {A} (l ll : list A),
    rev l = rev ll ->
    l = ll.
Proof.
  induction l using rev_ind.
  - induction ll using rev_ind; [ | clear IHll ].
    + intros. reflexivity.
    + intros. rewrite rev_app_distr in H. simpl in H. discriminate H.
  - intros. rewrite rev_app_distr in H. simpl in H.
    induction ll using rev_ind; [ | clear IHll ].
    + discriminate H.
    + rewrite rev_app_distr in H. simpl in H. injection H. intros. subst.
      rewrite (IHl _ H0). reflexivity.
Qed.

Require Import List.
Require Import Arith.PeanoNat.

Require Import Compare_lt_eq_lt_dec.
Require Import le_minus_O.

Lemma skipn_app :
  forall {A} n (l ll : list A),
    n <= length l ->
    skipn n (l ++ ll) = skipn n l ++ ll.
Proof.
  induction n; intros.
  - reflexivity.
  - destruct l.
    + inversion H.
    + simpl in H. assert (H' := le_S_n _ _ H).
      assert (IHn' := IHn _ ll H').
      simpl. assumption.
Qed.

Lemma skipn_app2 :
  forall {A} n (l ll : list A),
    length l <= n ->
    skipn n (l ++ ll) = skipn (n - length l) ll.
Proof.
  induction n; intros.
  - destruct l; [ | inversion H ]. simpl. reflexivity.
  - destruct l.
    + unfold app. unfold length. rewrite <- Minus.minus_n_O. reflexivity.
    + simpl in H. assert (H' := le_S_n _ _ H).
      assert (IHn' := IHn l ll H'). assert (length (a::l) = S (length l)); [ solve [ auto ] | ].
      rewrite H0. rewrite Nat.sub_succ. rewrite <- IHn'.
      simpl. reflexivity.
Qed.

Lemma firstn_all3 :
  forall {A} (l ll : list A),
    firstn (length l) (l ++ ll) = l.
Proof.
  induction l.
  - simpl. auto.
  - intros. simpl. apply f_equal. apply IHl.
Qed.

Lemma skipn_all :
  forall {A} (l : list A),
    skipn (length l) l = nil.
Proof.
  intros.
  assert (Hx := firstn_skipn (length l) (l ++ nil)).
  rewrite firstn_all3 in Hx. assert (Hy := app_inv_head _ _ _ Hx).
  rewrite app_nil_r in Hy.
  assumption.
Qed.

Lemma skipn_all2 :
  forall {A} (l ll : list A),
    skipn (length l) (l ++ ll) = ll.
Proof.
  intros.
  assert (Hx := firstn_skipn (length l) (l ++ ll)).
  rewrite firstn_all3 in Hx. assert (Hy := app_inv_head _ _ _ Hx).
  assumption.
Qed.

Lemma app_inv_len :
  forall {A} (l ll r rr : list A),
    l ++ ll = r ++ rr ->
    length l = length r ->
    l = r /\ ll = rr.
Proof.
  intros.
  assert (H' := H).
  rewrite <- (firstn_skipn (length l)) in H' at 1.
  assert (Hx := firstn_skipn (length l) (r ++ rr)). rewrite <- Hx in H'. clear Hx.
  split.
  - rewrite H in H' at 2.
    assert (Hx := app_inv_tail _ _ _ H'). rewrite H0 in Hx at 2. do 2 rewrite firstn_all3 in Hx.
    assumption.
  - rewrite H in H' at 1.
    assert (Hx := app_inv_head _ _ _ H'). rewrite H0 in Hx at 2. do 2 rewrite skipn_all2 in Hx.
    assumption.
Qed.

Lemma twosplit :
  forall {A} {l1 l2 r1 r2 : list A},
    l1 ++ l2 = r1 ++ r2 ->
    firstn (length r1) (l1 ++ l2) = r1 /\
    skipn (length r1) (l1 ++ l2) = r2.
Proof.
  intros. assert (Hx := firstn_skipn (length r1) (l1 ++ l2)).
  rewrite H in Hx at 3.
  rewrite H in Hx at 1.
  rewrite firstn_all3 in Hx. assert (Hy := app_inv_head _ _ _ Hx).
  split; [ | exact Hy ]. clear Hy.
  rewrite H. rewrite firstn_all3. reflexivity.
Qed.

Lemma twosplit'' :
  forall {A} {l1 l2 r1 r2 : list A},
    l1 ++ l2 = r1 ++ r2 ->
    (length r1 < length l1 /\
     firstn (length r1) l1 = r1 /\
     (skipn (length r1) l1) ++ l2 = r2) \/
    (length r1 = length l1 /\ l1 = r1 /\ l2 = r2) \/
    (length l1 < length r1 /\
     l1 ++ firstn (length r1 - length l1) l2 = r1 /\
     skipn (length r1 - length l1) l2 = r2).
Proof.
  intros. assert (Hx := twosplit H).
  destruct (Compare_lt_eq_lt_dec (length r1) (length l1)) as [ [|] | ].
  - left. split; [ assumption | split ].
    + destruct Hx as [ Hx _ ].
      rewrite firstn_app in Hx.
      match type of Hx with _ ++ ?n = _ => assert (n = nil) end.
      {
        match goal with |- firstn ?x _ = nil => assert (x = 0) end.
        {
          apply le_minus_O. unfold lt in l. apply Le.le_Sn_le. assumption.
        }
        rewrite H0. reflexivity.
      }
      rewrite H0 in Hx. rewrite app_nil_r in Hx. assumption.
    + destruct Hx as [ _ Hx ].
      assert (length r1 <= length l1). { unfold lt in l. apply Le.le_Sn_le. assumption. }
      assert (Hy := @skipn_app _ _ _ l2 H0).
      rewrite Hy in Hx. assumption.
  - right; left. split; [ assumption | ].
    assert (Hy := @app_inv_len _ _ _ _ _ H (eq_sym e)). assumption.
  - right; right. split; [ | split; [ destruct Hx as [ Hx _ ] | destruct Hx as [ _ Hx ] ] ].
    + assumption.
    + rewrite firstn_app in Hx.
      unfold lt in l.
      assert (Hy := firstn_all2 l1 (Le.le_Sn_le _ _ l)).
      rewrite Hy in Hx. exact Hx.
    + rewrite skipn_app2 in Hx; [ exact Hx | ].
      unfold lt in l. exact (Le.le_Sn_le _ _ l).
Qed.

Lemma twosplit' :
  forall {A} {l1 l2 r1 r2 : list A},
    l1 ++ l2 = r1 ++ r2 ->
    (firstn (length r1) l1 = r1 /\
     (skipn (length r1) l1) ++ l2 = r2) \/
    (l1 = r1 /\ l2 = r2) \/
    ((l1 ++ firstn (length r1 - length l1) l2) = r1 /\
     skipn (length r1 - length l1) l2 = r2).
Proof.
  intros.
  assert (Hx := twosplit'' H). destruct Hx as [ | [ | ] ]; [ left | right; left | right; right ]; apply H0.
Qed.

Lemma threesplit' :
  forall {A} (l1 l2 l3 r1 r2 : list A),
    l1 ++ l2 ++ l3 = r1 ++ r2 ->
    (firstn (length r1) l1 = r1 /\
     skipn (length r1) l1 ++ l2 ++ l3 = r2) \/
    (l1 ++ firstn (length r1 - length l1) l2 = r1 /\
     skipn (length r1 - length l1) l2 ++ l3 = r2) \/
    (l1 ++ l2 ++ firstn (length r1 - length l1 - length l2) l3 = r1 /\
     skipn (length r1 - length l1 - length l2) l3 = r2).
Proof.
  intros.
  assert (l1 ++ l2 ++ l3 = (l1 ++ l2) ++ l3); [ rewrite app_assoc; reflexivity | ].
  rewrite H0 in *. clear H0.
  destruct (twosplit'' H) as [ | [ | ] ]; [ | right; left | ].
  - destruct H0. destruct H1. subst r2.
    rewrite app_assoc in H. assert (Hx := app_inv_tail _ _ _ H). clear H.
    destruct (twosplit'' Hx) as [ | [ | ] ].
    + left. split; [ solve [ apply H ] | ].
      destruct H. destruct H2. rewrite <- H3.
      rewrite <- app_assoc. reflexivity.
    + left. destruct H as [ ? [] ]. subst.
      split.
      * rewrite firstn_all. reflexivity.
      * rewrite skipn_all2. rewrite <- app_nil_l. apply f_equal2; [ | reflexivity ].
        apply skipn_all.
    + right; left. split; [ solve [ apply H ] | ].
      destruct H. destruct H2. rewrite H3. reflexivity.
  - destruct H0. destruct H1. subst r1. subst r2.
    clear. split.
    + apply f_equal. rewrite app_length. rewrite Minus.minus_plus. rewrite firstn_all. reflexivity.
    + rewrite app_length. rewrite Minus.minus_plus. rewrite skipn_all. reflexivity.
  - destruct H0. destruct H1.
    right; right.
    split.
    + rewrite <- H1 at 2. rewrite app_assoc. apply f_equal.
      apply f_equal2; [ | reflexivity ].
      rewrite app_length. rewrite Nat.sub_add_distr. reflexivity.
    + rewrite <- H2. apply f_equal2; [ | reflexivity ].
      rewrite app_length. rewrite Nat.sub_add_distr. reflexivity.
Qed.

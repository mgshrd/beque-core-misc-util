(* importing List take ages *)

Remark List_app_nil_r :
  forall A (l : list A),
    (l ++ nil)%list = l.
Proof.
  induction l.
  - intros; reflexivity.
  - intros. simpl. rewrite IHl. reflexivity.
Qed.

Require Import List.

Lemma fold_left_extract_accu :
  forall A (f : A -> A -> A) l a i
         (leftid : forall e, f i e = e)
         (rightid : forall e, f e i = e)
         (fassoc : forall a b c, f a (f b c) = f (f a b) c),
    fold_left f l a = f a (fold_left f l i).
Proof.
  intros. revert dependent a.
  induction l.
  - simpl. intros. rewrite rightid. reflexivity.
  - intros.
    simpl. rewrite (IHl (f i a)). rewrite IHl.
    remember (fold_left _ _ _).
    rewrite leftid. rewrite fassoc. reflexivity.
Qed.

Lemma Nat_sub_succ :
  forall n m : nat, S n - S m = n - m.
Proof.
  destruct n; destruct m; reflexivity.
Qed.

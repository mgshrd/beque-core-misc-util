Require Import List.

Inductive reified_list_expression {A} :=
  | latom : list A -> reified_list_expression
  | rlatom : list A -> reified_list_expression
  | lcons : A -> reified_list_expression -> reified_list_expression
  | lrev : reified_list_expression -> reified_list_expression
  | lapp : reified_list_expression -> reified_list_expression -> reified_list_expression.

Fixpoint reified_list_expression_forall {A} (pred : @reified_list_expression A -> Prop) (rl : @reified_list_expression A) : Prop :=
  pred rl /\
  match rl with
    | latom _ => True
    | rlatom _ => True
    | lcons _ rt => reified_list_expression_forall pred rt
    | lrev rl => reified_list_expression_forall pred rl
    | lapp rl1 rl2 => reified_list_expression_forall pred rl1 /\
                      reified_list_expression_forall pred rl2
  end.
Lemma reified_list_expression_forall_unfold_lcons :
  forall A pred h rl,
    @reified_list_expression_forall A pred (lcons h rl) ->
    reified_list_expression_forall pred rl.
Proof.
  intros. simpl in H. destruct H. assumption.
Qed.
Lemma reified_list_expression_forall_unfold_lrev :
  forall A pred rl,
    @reified_list_expression_forall A pred (lrev rl) ->
    reified_list_expression_forall pred rl.
Proof.
  intros. simpl in H. destruct H. assumption.
Qed.
Lemma reified_list_expression_forall_unfold_lapp :
  forall A pred rl1 rl2,
    @reified_list_expression_forall A pred (lapp rl1 rl2) ->
    reified_list_expression_forall pred rl1 /\
    reified_list_expression_forall pred rl2.
Proof.
  intros. simpl in H. destruct H. assumption.
Qed.

Fixpoint reified_list_denote {A} (rl : reified_list_expression) : list A :=
  match rl with
    | latom l => l
    | rlatom l => rev l
    | lcons h rt => h::(reified_list_denote rt)
    | lrev rl => rev (reified_list_denote rl)
    | lapp rl1 rl2 => (reified_list_denote rl1) ++ (reified_list_denote rl2)
  end.

Definition reified_list_expression_not_cons {A} (rl : @reified_list_expression A) : Prop :=
  match rl with
    | lcons _ _ => False
    | _ => True
  end.
Definition reified_list_expression_wo_cons {A} :=
  @reified_list_expression_forall A reified_list_expression_not_cons.
Fixpoint reified_list_elim_cons {A} (rl : @reified_list_expression A) : @reified_list_expression A :=
  match rl with
    | latom l => latom l
    | rlatom l => rlatom l
    | lcons h rt => lapp (latom (h::nil)) (reified_list_elim_cons rt)
    | lrev rl' => lrev (reified_list_elim_cons rl')
    | lapp rl1 rl2 => lapp (reified_list_elim_cons rl1) (reified_list_elim_cons rl2)
  end.
Lemma reified_list_elim_cons_has_no_cons :
  forall A rl, reified_list_expression_wo_cons (@reified_list_elim_cons A rl).
Proof.
  intros. induction rl; unfold reified_list_expression_wo_cons; simpl; auto.
Qed.
Lemma reified_list_elim_cons_correct :
  forall A rl, @reified_list_denote A rl = reified_list_denote (reified_list_elim_cons rl).
Proof.
  induction rl; simpl; auto; try solve [ rewrite IHrl; reflexivity ].
  rewrite IHrl1, IHrl2. reflexivity.
Qed.

Definition reified_list_expression_not_rev {A} (rl : @reified_list_expression A) : Prop :=
  match rl with
    | lrev _ => False
    | _ => True
  end.
Definition reified_list_expression_wo_rev {A} :=
  @reified_list_expression_forall A reified_list_expression_not_rev.
Definition reified_list_expression_wo_rev_if_latom {A} {l : list A} {rl}
           (pf : rl = latom l) :
  reified_list_expression_wo_rev rl.
Proof.
  subst. unfold reified_list_expression_wo_rev. simpl. auto.
Defined.
Definition reified_list_expression_wo_rev_if_rlatom {A} {l : list A} {rl}
           (pf : rl = rlatom l) :
  reified_list_expression_wo_rev rl.
Proof.
  subst. unfold reified_list_expression_wo_rev. simpl. auto.
Defined.
Definition reified_list_expression_wo_rev_if_lcons {A} {h : A} {t rl}
           (pf : rl = lcons h t)
           (tpf : reified_list_expression_wo_rev t) :
  reified_list_expression_wo_rev rl.
Proof.
  subst. unfold reified_list_expression_wo_rev. simpl. auto.
Defined.
Definition reified_list_expression_wo_rev_if_lapp {A} {rl1 rl2 rl}
           (pf : rl = @lapp A rl1 rl2)
           (rl1pf : reified_list_expression_wo_rev rl1)
           (rl2pf : reified_list_expression_wo_rev rl2) :
  reified_list_expression_wo_rev rl.
Proof.
  subst. unfold reified_list_expression_wo_rev in *; simpl; auto.
Defined.
Fixpoint reified_list_elim_rev_b {A} (rl : @reified_list_expression A) (dorev : bool) : @reified_list_expression A :=
  let straight := fun l => latom l in
  let revved := fun l => rlatom l in
  let apped := fun rl1 rl2 => lapp rl1 rl2 in
  match rl with
    | latom l => if dorev then revved l else straight l (*reverse if dorev *)
    | rlatom l => if dorev then straight l else revved l (*reverse if dorev *)
    | lcons h rt =>
      (* split up cons and app in reverse or straight order *)
      let rt' := reified_list_elim_rev_b rt dorev in
      let h' := latom (h::nil) in
      if dorev
      then apped rt' h'
      else lcons h rt'
    | lrev l => reified_list_elim_rev_b l (negb dorev)
    | lapp rl1 rl2 =>
      let rl1' := reified_list_elim_rev_b rl1 dorev in
      let rl2' := reified_list_elim_rev_b rl2 dorev in
      if dorev
      then apped rl2' rl1'
      else apped rl1' rl2'
  end.
Definition reified_list_elim_rev {A} rl := @reified_list_elim_rev_b A rl false.
Lemma reified_list_elim_rev_idem_if_no_rev' :
  forall A rl, @reified_list_expression_wo_rev A rl -> rl = reified_list_elim_rev_b rl false.
Proof.
  induction rl; simpl; auto; intros.
  - intros. rewrite <- IHrl; [ reflexivity | ]. clear IHrl.
    apply reified_list_expression_forall_unfold_lcons with a. assumption.
  - intros. exfalso. unfold reified_list_expression_wo_rev in H.
    simpl in H. destruct H.
    elim H.
  - intros. unfold reified_list_expression_wo_rev in H. simpl in H. destruct H as [ _ [ p1 p2 ] ].
    rewrite <- IHrl1, <- IHrl2; try assumption.
    reflexivity.
Qed.
Lemma reified_list_elim_rev_b_has_no_rev :
  forall A rl b, @reified_list_expression_wo_rev A (reified_list_elim_rev_b rl b).
Proof.
  induction rl; try solve [ destruct b; unfold reified_list_elim_rev; simpl; unfold reified_list_expression_wo_rev; simpl; auto ].
  - destruct b.
    + simpl.
      assert (Hx := IHrl true). clear IHrl.
      match goal with |- reified_list_expression_wo_rev ?x => remember x end.
      apply (reified_list_expression_wo_rev_if_lapp Heqr); auto.
      unfold reified_list_expression_wo_rev. simpl. auto.
    + simpl. unfold reified_list_expression_wo_rev. simpl.
      split; [ auto | ].
      apply (IHrl false).
  - destruct b; simpl; apply IHrl.
  - destruct b;
    simpl;
    match goal with |- reified_list_expression_wo_rev ?x => remember x end;
    apply (reified_list_expression_wo_rev_if_lapp Heqr); auto.
Qed.
Lemma reified_list_elim_rev_has_no_rev :
  forall A rl, @reified_list_expression_wo_rev A (reified_list_elim_rev rl).
Proof. intros. apply (reified_list_elim_rev_b_has_no_rev A rl false). Qed.
Lemma rev_hom :
  forall A (l ll : list A), l = ll <-> rev l = rev ll.
Proof.
  induction l.
  - intros ll. split; intro Hyp; subst; auto.
    destruct ll. reflexivity.
    exfalso. simpl in Hyp. destruct (rev ll).
    + simpl in Hyp. discriminate Hyp.
    + simpl in Hyp. discriminate Hyp.
  - intros ll. split; intro Hyp. rewrite Hyp. reflexivity.
    destruct ll. simpl in Hyp. destruct (rev l). simpl in Hyp. discriminate Hyp. simpl in Hyp. discriminate Hyp.
    simpl in Hyp. destruct (app_inj_tail _ _ _ _ Hyp) as [ req heq ]. clear Hyp.
    rewrite <- IHl in req.
    subst. reflexivity.
Qed.
Lemma reified_list_elim_rev_b_correct :
  forall A rl (b : bool), (if b then @rev _ else id) (@reified_list_denote A rl) = reified_list_denote (reified_list_elim_rev_b rl b).
Proof.
  induction rl; intro b; try solve [ destruct b; simpl in *; auto ].
  - destruct b; simpl.
    + apply rev_involutive.
    + unfold id. reflexivity.
  - simpl. assert (IHrl' := IHrl b). clear IHrl. rename IHrl' into IHrl.
    destruct b; simpl; rewrite <- IHrl; unfold id; reflexivity.
  - assert (IHrl' := IHrl (negb b)). clear IHrl. rename IHrl' into IHrl.
    destruct b; simpl in *; unfold id in *; auto.
    rewrite rev_involutive. assumption.
  - assert (IHrl1' := IHrl1 b); assert (IHrl2' := IHrl2 b); clear IHrl1 IHrl2. rename IHrl1' into IHrl1, IHrl2' into IHrl2.
    destruct b; simpl; try rewrite rev_app_distr; rewrite <- IHrl1, <- IHrl2; reflexivity.
Qed.
Lemma reified_list_elim_rev_correct :
  forall A rl, @reified_list_denote A rl = reified_list_denote (reified_list_elim_rev rl).
Proof.
  intros. apply (reified_list_elim_rev_b_correct A rl false).
Qed.

Definition reified_list_expression_not_applapp {A} (rl : @reified_list_expression A) : Prop :=
  match rl with
    | lapp rl1 _ => match rl1 with
                      | lapp _ _ => False (* no app with app on the left*)
                      | _ => True
                    end
    | _ => True
  end.
Definition reified_list_expression_wo_applapp {A} :=
  @reified_list_expression_forall A reified_list_expression_not_applapp.
Definition reified_list_expression_not_app {A} (rl : @reified_list_expression A) : Prop :=
  match rl with
    | lapp _ _ => False
    | _ => True
  end.
Lemma reified_list_expression_not_applapp_rl1_not_app :
  forall A rl, @reified_list_expression_not_applapp A rl -> match rl with
                                                              | lapp rl1 _ => reified_list_expression_not_app rl1
                                                              | _ => True
                                                            end.
Proof.
  destruct rl; simpl; auto.
Qed.
Fixpoint rleapp {A} (rl1 rl2 : @reified_list_expression A) :=
  (* append flattened reified lists *)
  match rl1 with
    | lapp rl1' rl2' => lapp rl1' (rleapp rl2' rl2)
    | _ => lapp rl1 rl2
  end.
Fixpoint reified_list_elim_applapp {A} (rl : @reified_list_expression A) : @reified_list_expression A :=
  match rl with
    | latom l => rl
    | rlatom l => rl
    | lcons h t => lcons h (reified_list_elim_applapp t)
    | lrev rl => lrev (reified_list_elim_applapp rl)
    | lapp rl1 rl2 =>
      let rl1' := reified_list_elim_applapp rl1 in
      let rl2' := reified_list_elim_applapp rl2 in
      rleapp rl1' rl2'
  end.
Lemma reified_list_expression_wo_applapp_not_applapp :
  forall A rl, @reified_list_expression_wo_applapp A rl -> reified_list_expression_not_applapp rl.
Proof.
  unfold reified_list_expression_wo_applapp.
  induction rl; simpl in *; auto.
  intros. destruct rl1; auto. destruct H. elim H.
Qed.
Lemma reified_list_expression_rleapp_wo_applapps :
  forall A rl1 rl2,
    @reified_list_expression_wo_applapp A rl1 ->
    reified_list_expression_wo_applapp rl2 ->
    reified_list_expression_wo_applapp (rleapp rl1 rl2).
Proof.
  induction rl1; simpl; intros; unfold reified_list_expression_wo_applapp; simpl; auto.
  split; [ | split ].
  - destruct rl1_1; auto.
    unfold reified_list_expression_wo_applapp in H. simpl in H. destruct H as [ H _ ].
    elim H.
  - clear IHrl1_1 IHrl1_2 rl2 H0.
    unfold reified_list_expression_wo_applapp in H.
    assert (Hx := reified_list_expression_forall_unfold_lapp _ _ _ _ H).
    apply Hx.
  - unfold reified_list_expression_wo_applapp in H.
    assert (Hx := reified_list_expression_forall_unfold_lapp _ _ _ _ H).
    apply IHrl1_2. apply Hx. assumption.
Qed.
Lemma reified_list_elim_applapp_has_no_applapp :
  forall A rl, @reified_list_expression_wo_applapp A (reified_list_elim_applapp rl).
Proof.
  induction rl; try solve [simpl; unfold reified_list_expression_wo_applapp; simpl; auto ].
  simpl. unfold reified_list_expression_wo_applapp in *.
  assert (Hy := reified_list_expression_wo_applapp_not_applapp _ _ IHrl1).
  assert (Hx := reified_list_expression_not_applapp_rl1_not_app _ (reified_list_elim_applapp rl1) Hy). clear Hy.
  destruct (reified_list_elim_applapp rl1); simpl in *; auto.
  destruct IHrl1 as [ r1contra [ r1woapp r2woapp ] ].
  split; [ destruct r1; auto; contradiction | ].
  split; [ assumption | ].
  apply reified_list_expression_rleapp_wo_applapps. assumption. assumption.
Qed.
Lemma rleapp_correct :
  forall A rl1 rl2,
    @reified_list_denote A rl1 ++ reified_list_denote rl2 =
    reified_list_denote (rleapp rl1 rl2).
Proof.
  induction rl1; simpl; auto.
  intro. rewrite <- IHrl1_2. rewrite app_assoc. reflexivity.
Qed.
Lemma reified_list_elim_applapp_correct :
  forall A rl, @reified_list_denote A rl = reified_list_denote (reified_list_elim_applapp rl).
Proof.
  induction rl; simpl; auto.
  - rewrite IHrl. reflexivity.
  - rewrite IHrl. reflexivity.
  - rewrite IHrl1, IHrl2. clear IHrl1 IHrl2.
    apply rleapp_correct.
Qed.

Lemma reified_list_elim_cons_adds_no_rev :
  forall A rl, @reified_list_expression_wo_rev A rl -> reified_list_expression_wo_rev (reified_list_elim_cons rl).
Proof.
  induction rl; simpl in *; auto.
  - intro H. unfold reified_list_expression_wo_rev in *.
    assert (Hx := IHrl (reified_list_expression_forall_unfold_lcons _ _ _ _ H)).
    simpl. auto.
  - intro H. unfold reified_list_expression_wo_rev in *.
    simpl in H. destruct H as [ contra _ ]. elim contra.
  - intro H. unfold reified_list_expression_wo_rev in *.
    assert (Hx := IHrl1 (proj1 (reified_list_expression_forall_unfold_lapp _ _ _ _ H))).
    assert (Hy := IHrl2 (proj2 (reified_list_expression_forall_unfold_lapp _ _ _ _ H))).
    simpl. auto.
Qed.
Lemma rleapp_adds_no_rev :
  forall A rl1 rl2,
    @reified_list_expression_wo_rev A rl1 ->
    reified_list_expression_wo_rev rl2 ->
    reified_list_expression_wo_rev (rleapp rl1 rl2).
Proof.
  unfold reified_list_expression_wo_rev.
  induction rl1; simpl; auto.
  intros. split; [ auto | split ].
  - apply H.
  - apply IHrl1_2.
    + apply H.
    + apply H0.
Qed.
Lemma reified_list_elim_applapp_adds_no_rev :
  forall A rl, @reified_list_expression_wo_rev A rl -> reified_list_expression_wo_rev (reified_list_elim_applapp rl).
Proof.
  induction rl; simpl in *; auto.
  - intro H. unfold reified_list_expression_wo_rev in *.
    assert (Hx := IHrl (reified_list_expression_forall_unfold_lcons _ _ _ _ H)).
    simpl. auto.
  - intro H. unfold reified_list_expression_wo_rev in *.
    simpl in H. destruct H as [ contra _ ]. elim contra.
  - intro H. unfold reified_list_expression_wo_rev in *.
    assert (Hx := IHrl1 (proj1 (reified_list_expression_forall_unfold_lapp _ _ _ _ H))).
    assert (Hy := IHrl2 (proj2 (reified_list_expression_forall_unfold_lapp _ _ _ _ H))).
    apply rleapp_adds_no_rev; auto.
Qed.
Lemma rleapp_adds_no_cons :
  forall A rl1 rl2,
    @reified_list_expression_wo_cons A rl1 ->
    reified_list_expression_wo_cons rl2 ->
    reified_list_expression_wo_cons (rleapp rl1 rl2).
Proof.
  unfold reified_list_expression_wo_cons.
  induction rl1; simpl; auto.
  intros. split; [ auto | split ].
  - apply H.
  - apply IHrl1_2.
    + apply H.
    + apply H0.
Qed.
Lemma reified_list_elim_applapp_adds_no_cons :
  forall A rl, @reified_list_expression_wo_cons A rl -> reified_list_expression_wo_cons (reified_list_elim_applapp rl).
Proof.
  induction rl; simpl in *; auto;
  intro H; unfold reified_list_expression_wo_cons in *.
  - simpl in H. destruct H as [ contra _ ]. elim contra.
  - simpl in *. split; [ auto | ]. apply IHrl. apply H.
  - assert (Hx := IHrl1 (proj1 (reified_list_expression_forall_unfold_lapp _ _ _ _ H))).
    assert (Hy := IHrl2 (proj2 (reified_list_expression_forall_unfold_lapp _ _ _ _ H))).
    apply rleapp_adds_no_cons; auto.
Qed.
Definition reified_list_normalize {A} (rl : @reified_list_expression A) : @reified_list_expression A := reified_list_elim_applapp (reified_list_elim_cons (reified_list_elim_rev rl)).
Lemma reified_list_normalize_correct :
  forall A rl, @reified_list_denote A rl = reified_list_denote (reified_list_normalize rl).
Proof.
  intros. unfold reified_list_normalize.
  rewrite <- reified_list_elim_applapp_correct.
  rewrite <- reified_list_elim_cons_correct.
  rewrite <- reified_list_elim_rev_correct.
  reflexivity.
Qed.

Ltac reify_list_expr le :=
  match le with
    | ?l1 ++ ?l2 =>
      let rl1 := reify_list_expr l1 in
      let rl2 := reify_list_expr l2 in
      constr:(lapp rl1 rl2)
    | ?h::?t =>
      let rt := reify_list_expr t in
      constr:(lcons h rt)
    | rev ?l =>
      let rl := reify_list_expr l in
      constr:(lrev rl)
    | ?l => constr:(latom l)
  end.
Ltac list_norm le H :=
  let rl := reify_list_expr le in
  assert (le = reified_list_denote rl) as H; [ simpl; reflexivity | ];
  let HH := fresh in
  assert (HH := reified_list_normalize_correct _ rl);
  let HHH := fresh in
  let HHHeq := fresh in
  remember (reified_list_denote rl) as HHH eqn:HHHeq;
  (*simpl in HH;*)
  unfold reified_list_normalize in HH;
  unfold reified_list_elim_rev in HH;
  unfold reified_list_elim_rev_b in HH;
  unfold negb in HH;
  unfold reified_list_elim_cons in HH;
  unfold reified_list_elim_applapp in HH;
  unfold rleapp in HH;
  unfold reified_list_denote in HH;
  repeat rewrite app_nil_r in HH; repeat rewrite app_nil_l in HH;
  rewrite HH in H; clear HH HHH HHHeq.

Tactic Notation "list_norm" constr(x) "as" ident(y) := list_norm x y.
Tactic Notation "list_norm" constr(x) :=
  let y := fresh "norm" in
  list_norm x as y.
Tactic Notation "list_norm_subst" constr(x) :=
  let y := fresh "tmp" in
  list_norm x as y; rewrite y in *; clear y.

Goal forall a (l ll lll : list nat), a::l ++ ((l ++ ll) ++ lll) ++ nil = a::nil ++ l ++ ((rev (rev ll ++ rev l)) ++ lll).
Proof.
intros.
list_norm (rev (a::l ++ rev (nil ++ l ++ nil ++ rev lll ++ nil ++ nil ++ ll) ++ (l ++ l) ++ ll)).
list_norm (nil ++ l).
list_norm (l ++ nil).
list_norm (@rev nat nil).
list_norm (l ++ rev (lll ++ ll ++ a::nil)).
list_norm (a::a::a::a::a::l) as sdfoib.
list_norm ((a::(rev ((a::l) ++ ll)) ++ lll)).
match goal with |- ?x = _ => list_norm x as ls end.
match goal with |- _ = ?x => list_norm x as rs end.
rewrite ls. rewrite rs. reflexivity.
Qed.

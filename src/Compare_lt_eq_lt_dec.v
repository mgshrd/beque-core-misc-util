Lemma Compare_lt_eq_lt_dec :
  forall n m : nat, {n < m} + {n = m} + {m < n}.
Proof.
  induction n.
  - destruct m.
    + left. right. reflexivity.
    + left. left. unfold lt. apply le_n_S. induction m; constructor; assumption.
  - intros. destruct m.
    + right. unfold lt. apply le_n_S. clear IHn. induction n; constructor; assumption.
    + destruct (IHn m) as [ [ | ] | ].
      * left. left.
        unfold lt in *. apply le_n_S. assumption.
      * left; right; subst; reflexivity.
      * right. unfold lt in *. apply le_n_S. assumption.
Qed.

Require Import sigS.
Require Import case_tag.

Ltac injit H := injection H; clear H; intros; subst.

Ltac magic_case x := generalize (refl_equal x); pattern x at -1; case x; intros.

Ltac rebuildx__ xx :=
  match xx with
  | left ?x => match type of x with
               | ?xt => let x' := rebuildx__ xt in
                        let res := fresh "left " x' in
                        res
               end
  | right ?x => match type of x with
                | ?xt => let x' := rebuildx__ xt in
                         let res := fresh "left " x' in
                         res
                end
  | @or_introl ?x _ _ => let res := rebuildx__ x in
                        res
  | @or_intror _ ?x _ => let res := rebuildx__ x in
                        res
  | ?x <> ?y => let x' := rebuildx__ x in
                let y' := rebuildx__ y in
                let res := fresh x' " <> " y' in
                res
  | ?x = ?y => let x' := rebuildx__ x in
               let y' := rebuildx__ y in
               let res := fresh x' " = " y' in
               res
  | ?y ?x => let y' := rebuildx__ y in
             let res := fresh y' " " x in
             res
  | ?y ?x => let y' := rebuildx__ y in
             let x' := rebuildx__ x in
             let res := fresh y' " (" x' ")" in
             res
  | _ => xx
  end.
Ltac mcase x :=
  let x' := fresh "mcase_tmp__" in
  let H := fresh in
  first [ rename x into x'; (* move old variable out of the way for new name allocations *)
          destruct x' eqn:H
        | destruct x eqn:H (* not a variable is destructed *)
        ];
  match type of H with
  | x' = ?y => let y' := rebuildx__ y in
               let msg := fresh x " = " y' in
               SG msg; clear H; try clear x'
  | x = ?y => let y' := rebuildx__ y in
              let x'' := rebuildx__ x in
              let msg := fresh x'' " = " y' in
              SG msg; clear H
  end.

Ltac nilapp x := match type of x with
                   | app ?a ?b = nil => let anil := fresh in
                                        let bnil := fresh in
                                        destruct a; [ | discriminate x ];
                                        destruct b; [ | discriminate x ]
                   | nil = app ?a  ?b => nilapp (eq_sym x)
                 end.

(** Remove non-informative hypotheses. *)
Ltac cleartriv := match goal with
                    | H : True |- _ => clear H; cleartriv
                    | H : ?x = ?x |- _ => clear H; cleartriv
                    | |- _ => idtac
                  end.

(** Destruct nested [{_ : _ & _}] strucutres. *)
Ltac desig :=
  repeat match goal with
         | H : sigS _ |- _ => destruct H; try subst
         | H : {_ : _ & _} |- _ => destruct H; try subst
         end.

(** Recursively destruct conjunctions, and remove non-informative hypotheses. *)
Ltac explode H := match type of H with
                    | _ /\ _ =>
                      let H0 := fresh in
                      destruct H as [ H H0 ];
                      cleartriv;
                      try explode H;
                      try explode H0
                  end.

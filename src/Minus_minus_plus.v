Lemma Minus_minus_plus :
  forall n m : nat, n + m - n = m.
Proof.
  induction n; destruct m; simpl; auto.
Qed.

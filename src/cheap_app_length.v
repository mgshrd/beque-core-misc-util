Require Import cheap_app_nil_r.
Lemma PeanoNat_Nat_add_comm : forall n m, n + m = m + n.
Proof.
  induction n; induction m; auto.
  simpl. rewrite <- IHm. simpl. rewrite IHn. simpl. rewrite IHn. reflexivity.
Qed.

Lemma List_app_length : forall (A : Type) (l l' : list A), length (l ++ l') = length l + length l'.
Proof.
  induction l; [ reflexivity | ].
  destruct l'.
  - rewrite List_app_nil_r. rewrite PeanoNat_Nat_add_comm. reflexivity.
  - simpl. rewrite IHl. simpl. reflexivity.
Qed.

Lemma length_omega :
  forall {A} {l : list A} {n},
    l <> nil ->
    n + length l = n ->
    False.
Proof.
  induction n.
  - intros. simpl in H0. destruct l.
    + elim H. reflexivity.
    + simpl in H0. discriminate.
  - intros. simpl in H0. assert (Hx := IHn H).
    assert (n + length l = n).
    { inversion H0. elim Hx. assumption. }
    elim Hx. assumption.
Qed.

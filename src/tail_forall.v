Require Import List.

Inductive TailForall
          {A : Type}
          (P : A -> list A -> Prop) :
  list A -> Prop :=
| TailForall_nil :
    TailForall P nil
| TailForall_cons :
    forall
      (x : A)
      (l : list A),
      P x l ->
      TailForall P l ->
      TailForall P (x::l).

Inductive TailExists
          {A : Type}
          (P : A -> list A -> Prop) :
  list A -> Prop :=
| TailExists_cons_hd :
    forall
      (x : A)
      (l : list A),
      P x l ->
      TailExists P (x :: l)
| TailExists_cons_tl :
    forall
      (x : A)
      (l : list A),
      TailExists P l ->
      TailExists P (x :: l).

Lemma TailForall_covers_FOP :
  forall A R (l : list A), ForallOrdPairs R l <-> TailForall (fun a l => Forall (R a) l) l.
Proof.
  induction l.
  { split; intro H; constructor. }
  split; intro H.
  - constructor.
    + inversion H. assumption.
    + rewrite <- IHl. inversion H. assumption.
  - constructor.
    + inversion H. assumption.
    + inversion H. rewrite IHl. assumption.
Qed. (* TODO prove that there are lists, that TF can and FOP cannot differentiate *)

(*
Lemma TailForall_forall :
  forall (A : Type) P (l : list A),
    TailForall P l <-> (forall x : A, In x l -> P x).
Proof.
Qed.*)

Lemma TailForall_inv_hd :
  forall (A : Type) P (a : A) (l : list A),
    TailForall P (a :: l) -> P a l.
Proof.
  intros. inversion H. exact H2.
Qed.

Lemma TailForall_inv_tl :
  forall (A : Type) P (a : A) (l : list A),
    TailForall P (a :: l) -> TailForall P l.
Proof.
  intros. inversion H. exact H3.
Qed.

Definition TailForall_dec :
  forall (A : Type) P,
    (forall x l, {P x l} + {~P x l}) ->
    forall l : list A, {TailForall P l} + {~TailForall P l}.
Proof.
  intros. induction l.
  { left. constructor. }
  destruct (X a l), IHl; [
      left; constructor; assumption |
      right |
      right |
      right
    ]; intro H; inversion H;
  match goal with
      H : ?x, HH : ~?x
      |- _
      => elim HH; exact H
  end.
Defined.

Lemma TailForall_TailExists_neg :
  forall (A : Type) P (l : list A),
    TailForall (fun x t => ~P x t) l <-> ~TailExists P l.
Proof.
  induction l.
  {
    split; intro H.
    - intro. inversion H0.
    - constructor.
  }
  split; intro H.
  - intro. inversion H; inversion H0.
    + elim H3. exact H6.
    + rewrite IHl in H4. elim H4. exact H6.
  - constructor.
    + intro. elim H. apply TailExists_cons_hd. exact H0.
    + rewrite IHl. intro. elim H. apply TailExists_cons_tl. exact H0.
Qed.

Lemma TailExists_TailForall_neg :
  forall
    (A : Type)
    P
    (l : list A),
    (forall x l, P x l \/ ~P x l) ->
    TailExists (fun x l => ~P x l) l <-> ~TailForall P l.
Proof.
  intros. induction l.
  {
    split; intro H0.
    - intro. inversion H0.
    - elim H0. constructor.
  }
  split; intro H0.
  - intro.
    inversion H1. subst.
    inversion H0; subst.
    + exact (H3 H4).
    + rewrite IHl in H3. exact (H3 H5).
  - destruct (H a l).
    + apply TailExists_cons_tl. rewrite IHl. intro H2. elim H0. constructor; assumption.
    + apply TailExists_cons_hd. exact H1.
Qed.

Definition TailForall_TailExists_dec :
  forall A P,
    (forall x l, {P x l}+{~P x l}) ->
    forall l : list A,
      {TailForall P l}+{TailExists (fun x l => ~P x l) l}.
Proof.
  induction l.
  { left. constructor. }
  destruct (X a l); [
      destruct IHl; [ left | right ] |
      right; apply TailExists_cons_hd; assumption
    ].
  - constructor; assumption.
  - apply TailExists_cons_tl. assumption.
Defined.

Lemma TailForall_impl :
  forall
    A
    (P Q : A -> list A -> Prop),
  (forall a l, P a l -> Q a l) ->
  (forall l : list A,
     TailForall P l ->
     TailForall Q l).
Proof.
  induction l.
  { intros _. constructor. }
  intro H0.
  inversion H0. subst.
  constructor.
  - apply H. exact H3.
  - apply IHl. exact H4.
Qed.

Lemma TailExists_exists :
  forall A P (l : list A),
    TailExists P l <-> (exists h x t, h ++ x::t = l /\ P x t).
Proof.
  induction l.
  {
    split; intro H.
    - inversion H.
    - exfalso.
      destruct H as [ h [ x [ t [ ppf _ ] ] ] ].
      destruct h; discriminate ppf.
  }
  split; intro H.
  - inversion H; subst.
    + exists nil. exists a. exists l. split; [ reflexivity | exact H1 ].
    + rewrite IHl in H1. clear IHl.
      destruct H1 as [ h [ x [ t [ ppf Ppf ] ] ] ]. subst l.
      exists (a::h). exists x. exists t.
      split; [ reflexivity | assumption ].
  - destruct H as [ h [ x [ t [ ppf Ppf ] ] ] ].
    destruct h.
    + apply TailExists_cons_hd.
      injection ppf. intros. subst. assumption.
    + apply TailExists_cons_tl.
      rewrite IHl.
      exists h. exists x. exists t.
      split; [ | exact Ppf ].
      simpl in ppf. injection ppf. intros. rewrite H. reflexivity.
Qed.

Lemma TailExists_nil :
  forall (A : Type) (P : A -> list A -> Prop), TailExists P nil <-> False.
Proof.
  intros; split; intro H; [ inversion H | contradiction H ].
Qed.

Lemma Exists_cons :
  forall (A : Type) P (x : A) (l : list A),
    TailExists P (x :: l) <-> P x l \/ TailExists P l.
Proof.
  intros.
  split; intro.
  - inversion H; subst.
    + left. exact H1.
    + right. exact H1.
  - destruct H.
    + apply TailExists_cons_hd. exact H.
    + apply TailExists_cons_tl. exact H.
Qed.

Definition Exists_dec
           (A : Type)
           P
           (l : list A) :
  (forall x l, {P x l} + {~P x l}) ->
  {TailExists P l} + {~TailExists P l}.
Proof.
  intros.
  induction l.
  { right. intro H. inversion H. }
  destruct (X a l); [
      left; apply TailExists_cons_hd; assumption |
      destruct IHl; [ left | right ]
    ].
  - apply TailExists_cons_tl. assumption.
  - intro H. inversion H; subst.
    + elim n. exact H1.
    + elim n0. exact H1.
Defined.

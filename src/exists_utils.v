Lemma exists_flip :
  forall {A B P},
    (exists (a : A) (b : B),  P a b) <->
    (exists b a, P a b).
Proof.
  firstorder.
Qed.

Lemma exists_flip3 :
  forall {A B C P},
    (exists (a : A) (b : B) (c : C),  P a b c) <->
    (exists c a b, P a b c).
Proof.
  firstorder.
Qed.

Lemma exists_indep :
  forall {A P Q},
    (exists (a : A), Q /\ P a) <->
    (Q /\ exists a, P a).
Proof.
  firstorder.
Qed.

Definition forall_exists
           {A B} {P : A -> B -> Prop}
           (H : exists a, forall b, P a b)
  : (forall b : B, exists a : A, P a b) :=
  match H with
  | ex_intro _ a v => fun b => ex_intro _ a (v b)
  end.

Require Import List.

Lemma fold_right_unfold :
  forall A B (f : B -> A -> A) a e l,
    fold_right f a (e::l) = f e (fold_right f a l).
Proof.
  intros. simpl. reflexivity.
Qed.

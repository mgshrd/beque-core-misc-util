Require Import Nat_sub_succ.

Lemma le_minus_O :
  forall n m,
    n <= m -> n - m = 0.
Proof.
  intros. revert n H. induction m.
  - intros. inversion H. auto.
  - intros. destruct n.
    + auto.
    + specialize IHm with n.
      assert (IHm' := IHm (le_S_n _ _ H)).
      rewrite Nat_sub_succ. assumption.
Qed.

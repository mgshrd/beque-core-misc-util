Require Import DecTypeHolder.
Require Import SetHolder.

Module Type DecSetHolder
<: DecTypeHolder
<: SetHolder
.

Parameter T : Set.
Parameter eq_T_dec :
  forall t1 t2 : T,
    {t1=t2}+{t1<>t2}.

End DecSetHolder.

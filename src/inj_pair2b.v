Require Import Eqdep_dec.

Definition depeq'
           {A P}
           {a1 a2 : A}
           (eqpf : a1 = a2)
           (x : P a1) (y : P a2) :
  Prop.
Proof.
  subst. exact (x = y).
Defined.

Definition depeq
           {A P}
           (dec : forall a1 a2 : A, {a1=a2}+{a1<>a2})
           {a1 a2 : A}
           (x : P a1) (y : P a2) :
  Prop.
Proof.
  destruct (dec a1 a2); [ | exact False ].
  subst. exact (x = y).
Defined.

Lemma de'2de :
  forall A P dec a1 a2 eqpf x y,
    @depeq' A P a1 a2 eqpf x y -> depeq dec x y.
Proof.
  intros. unfold depeq. unfold depeq' in H.
  subst. destruct dec.
  - unfold eq_rect_r in *. cut (e = eq_refl).
    + intro. subst. exact H.
    + apply (UIP_dec dec).
  - elim n. reflexivity.
Qed.

Lemma de2de' :
  forall A P dec a1 a2 x y,
    @depeq A P dec a1 a2 x y -> {eqpf | depeq' eqpf x y}.
Proof.
  intros. unfold depeq'. unfold depeq in H.
  destruct dec.
  - unfold eq_rect_r in *. exists e. exact H.
  - elim H.
Qed.

Lemma depeq_eq_a :
  forall {A P} {dec} {a1 a2 : A} {x : P a1} {y : P a2}, depeq dec x y -> a1 = a2.
Proof.
  intros. unfold depeq in H. destruct dec in H; [ | contradiction ].
  assumption.
Qed.

(* NB: the inverse is not true
Lemma not_depeq_neq_a :
  forall {A P} {dec} {a1 a2 : A} {x : P a1} {y : P a2}, ~depeq dec x y -> a1 <> a2.*)

Lemma depeq_eq :
  forall {A P} {dec} {a : A} {x y : P a}, depeq dec x y <-> x = y.
Proof.
  intros. split; intro.
  - unfold depeq in H. unfold eq_rect_r in H. destruct dec; [ | contradiction ].
    rewrite <- (eq_rect_eq_dec dec) in H. exact H.
  - subst. unfold depeq. destruct dec; [ | elim n; reflexivity ].
    unfold eq_rect_r. rewrite <- (eq_rect_eq_dec dec). reflexivity.
Qed.

Ltac depeq_to_eq H :=
  let aeq := fresh in
  let veq := fresh in
  assert (aeq := depeq_eq_a _ _ _ _ _ H);
  assert (veq := H);
  match type of aeq with ?x = ?y => subst y end;
  assert (aeq := depeq_eq _ _ _ _ veq);
  clear veq H;
  rename aeq into H.



Require Import EqdepFacts.



Lemma eq_a_existT_projT2 :
  forall
    X Y
    (x1 x2 : X) (y : Y x1) (y' : Y x2)
    (se : existT _ x1 y = existT _ x2 y'),
    x1 = x2.
Proof.
  intros. inversion se. reflexivity.
Qed.

Lemma eq_existT_projT2 :
  forall
    X (xdec : forall x1 x2 : X, {x1=x2}+{x1<>x2})
    Y
    (x : X) (y y' : Y x)
    (se : existT _ x y = existT _ x y'),
    y = y'.
Proof.
  intros.
  set (eq_rect_eq := eq_rect_eq_dec xdec).
  set (eq_dep_eq := eq_rect_eq__eq_dep_eq _ eq_rect_eq). unfold Eq_dep_eq in eq_dep_eq. unfold Eq_dep_eq_on in eq_dep_eq.
  set (inj_pairT2 := eq_dep_eq__inj_pairT2 _ eq_dep_eq). unfold Inj_dep_pairT in inj_pairT2. unfold Inj_dep_pair_on in inj_pairT2.
  compute in inj_pairT2.
  apply inj_pairT2. exact se.
Qed.

Require Import TypeHolder.

Module Type DecTypeHolder
<: TypeHolder
.

Parameter T : Type.
Parameter eq_T_dec :
  forall t1 t2 : T,
    {t1=t2}+{t1<>t2}.

End DecTypeHolder.

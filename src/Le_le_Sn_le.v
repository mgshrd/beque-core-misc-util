Lemma Le_le_Sn_le :
  forall n m : nat, S n <= m -> n <= m.
Proof.
  intros. destruct m.
  - inversion H.
  - constructor. apply le_S_n. assumption.
Qed.

(** limit sigT to Set *)
Definition sigS
           {A : Set}
           (P : A -> Set) :
  Set
  := sigT P.

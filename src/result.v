Inductive result (OK ERR : Set) :
  Set :=
| ok : OK -> result OK ERR
| err : ERR -> result OK ERR.
Arguments ok {_} {_} _.
Arguments err {_} {_} _.

Definition eq_result_dec
           {O E : Set}
           (eq_ok_dec : forall o1 o2 : O, {o1=o2}+{o1<>o2})
           (eq_err_dec : forall e1 e2 : E, {e1=e2}+{e1<>e2}) :
  forall r1 r2 : result O E, {r1=r2}+{r1<>r2}.
Proof.
  decide equality.
Defined.

Definition is_ok
           {O E}
           (r : result O E) :
  Prop
  := match r with
       | ok _ => True
       | err _ => False
     end.
Definition is_ok_dec
           {O E}
           (r : result O E) :
  {is_ok r}+{~is_ok r}.
Proof.
  destruct r; firstorder.
Defined.

Definition is_err
           {O E}
           (r : result O E) :
  Prop
  := match r with
       | ok _ => False
       | err _ => True
     end.
Definition is_err_dec
           {O E}
           (r : result O E) :
  {is_err r}+{~is_err r}.
Proof.
  destruct r; firstorder.
Defined.

Lemma is_ok_ne_is_err :
  forall {O E} (r : result O E),
    is_ok r <-> ~is_err r.
Proof.
  intros; split; intros; [ intro | ]; destruct r; firstorder.
Qed.

Lemma is_err_ne_is_ok :
  forall {O E} (r : result O E),
    is_err r <-> ~is_ok r.
Proof.
  intros; split; intros; [ intro | ]; destruct r; firstorder.
Qed.

Definition ok_map
           {O O' : Set} (f : O -> O')
           {E} (r : result O E) :
  result O' E :=
  match r with
    | ok v => ok (f v)
    | err e => err e
  end.

Definition err_map
           {E E' : Set} (f : E -> E')
           {O}  (r : result O E) :
  result O E' :=
  match r with
    | ok v => ok v
    | err e => err (f e)
  end.

Definition get_ok_err
           {O E} (r : result O E) :
  option O :=
  match r with
    | ok v => Some v
    | err e => None
  end.

Definition get_err_err
           {O E} (r : result O E) :
  option E :=
  match r with
    | ok v => None
    | err e => Some e
  end.

Definition get_ok {O E} {r : result O E} {v : O} (pf : r = ok v) : O := v.
Definition get_err {O E} {r : result O E} {e : E} (pf : r = err e) : E := e.

Definition flatten
           {O E E'} (r : result (result O E) E') :
  result O (E + E') :=
  match r with
    | ok v => err_map (fun ec => inl ec) v
    | err ec => err (inr ec)
  end.

Definition flatten_err
           {O E E'} (r : result O (result E E')) :
  result O (E + E') :=
  match r with
    | ok v => ok v
    | err ec => err match ec with
                          | ok e => inl e
                          | err e => inr e
                        end
  end.

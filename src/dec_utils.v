Require Import ltac_utils.

Definition eq_bool_dec
           (b1 b2 : bool) :
  {b1=b2}+{b1<>b2} :=
  ltac:(decide equality).

Definition eq_unit_dec
           (u1 u2 : unit) :
  {u1=u2}+{u1<>u2} :=
  ltac:(decide equality).

Definition eq_prod_dec :
  forall
    {A B}
    (p1 p2 : A * B)
    (eq_A_dec : forall a1 a2 : A, {a1=a2}+{a1<>a2})
    (eq_B_dec : forall b1 b2 : B, {b1=b2}+{b1<>b2}),
    {p1=p2}+{p1<>p2}.
Proof.
  decide equality.
Defined.

Definition eq_sum_dec :
  forall
    {A B}
    (eq_A_dec : forall a1 a2 : A, {a1=a2}+{a1<>a2})
    (eq_B_dec : forall b1 b2 : B, {b1=b2}+{b1<>b2}),
  forall ab1 ab2 : A + B, {ab1=ab2}+{ab1<>ab2}.
Proof.
  intros. decide equality.
Defined.

Definition eq_option_dec :
  forall
    {A}
    (eq_A_dec : forall a1 a2 : A, {a1=a2}+{a1<>a2})
    (o1 o2 : option A),
    {o1=o2}+{o1<>o2}.
Proof.
  decide equality.
Defined.

(* avoid including List. *)
Definition List_list_eq_dec :
  forall {A} (eq_A_dec : forall a1 a2 : A, {a1=a2}+{a1<>a2}),
  forall l1 l2 : list A, {l1=l2}+{l1<>l2}.
Proof.
  intros. decide equality.
Defined.

Definition PeanoNat_Nat_eq_dec :
  forall (n m : nat), {n = m} + {n <> m}.
Proof.
  intros. decide equality.
Defined.

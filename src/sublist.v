Definition nil_dec :
  forall {A} (l : list A), {l = nil}+{l <> nil}.
Proof. destruct l; [ left; reflexivity | right; intro d; discriminate d ]. Defined.

Definition postfix_of {A} (p l : list A) := exists h, (h ++ p)%list = l.

Definition proper_postfix_of {A} (p l : list A) := exists h, p <> nil /\ p <> l /\ (h ++ p)%list = l.

Definition prefix_of {A} (p l : list A) := exists t, (p ++ t)%list = l.

Definition proper_prefix_of {A} (p l : list A) := exists t, p <> nil /\ p <> l /\ (p ++ t)%list = l.

Definition sublist_of {A} (s l : list A) := exists pre post, (pre ++ s ++ post)%list = l.

Definition proper_sublist_of {A} (s l : list A) := exists pre post, s <> nil /\ s <> l /\ (pre ++ s ++ post)%list = l.

Definition overlap {A} (l ll : list A) := exists p i q, i <> nil /\ l = (p ++ i)%list /\ ll = (i ++q)%list.

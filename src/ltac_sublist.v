Require Import sublist_facts.
Require Import case_tag.
Require Import cheap_app_length.

Ltac pfelim xxx :=
  match type of xxx with
  | (?x ++ _)%list = (?y ++ _)%list =>
    let xsuby := fresh x "sub" y in
    let xeqy := fresh x "eq" y in
    let ysubx := fresh y "sub" x in
    destruct (prefix_of_app_elim3way xxx) as [ [ ? [ ? [ ? ? ] ] ] | [ [ ? ? ] | [ ? [ ? [ ? ? ] ] ] ] ]
  end.

Ltac pfelim5 xxx :=
  match type of xxx with
  | (?x ++ ?z)%list = (?y ++ _)%list =>
    let xnil := fresh x "nil" in
    let xsuby := fresh x "sub" y in
    let xeqy := fresh x "eq" y in
    let ysubx := fresh y "sub" x in
    let znil := fresh z "nil" in
    destruct (prefix_of_app_elim5way xxx)
      as [ [ ? ? ]
         | [ [ ? [ ? [ ? [ ? ? ] ] ] ]
         | [ [ ? ? ]
         | [ [ ? [ ? [ ? [ ? ? ] ] ] ]
         | [ ? ? ] ] ] ] ]
  end.

Ltac lengthelim xxx :=
  match type of xxx with
  | ?x = ?y =>
    let leq := fresh in
    assert (length x = length y) as leq; [ rewrite <- xxx; reflexivity | ];
    repeat rewrite List_app_length in leq; simpl in leq;
    repeat rewrite List_app_length in leq; simpl in leq
  end.

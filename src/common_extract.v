Require Extraction.

Require Import Ascii.

Require Import result.

Require Import DelayExtractionDefinitionToken.

Require Import ZArith.

Module commonExtractHaskell (delay : DelayExtractionDefinitionToken).

Extract Inductive sigT => "(,)" [ "(,)" ].
Extract Inductive unit => "()" [ "()" ].
Extract Inductive bool => "Prelude.Bool" [ "Prelude.True" "Prelude.False" ].
Extract Inductive sumbool => "Prelude.Bool" [ "Prelude.True" "Prelude.False" ].
Extract Inductive option => "Prelude.Maybe" [ "Prelude.Just" "Prelude.Nothing" ].
Extract Inductive prod => "(,)" [ "(,)" ].
Extract Inductive list => "[]" [ "[]" "(:)" ].

Extract Inductive result => "BequeResult" [ "Prelude.Right" "Prelude.Left" ]. (* Haskell convention *)

Extract Inductive Ascii.ascii => "Prelude.Char" ["ascii"]. (* extraction magically maps to native char *)
Extract Inlined Constant Ascii.ascii_dec => "(Prelude.==)".
(* extracting like this would make Prelude.String ambiguous: multibyte characters are extracted into single byte constituents, resulting in possibly illegal Prelude.String. The result is more like Data.ByteString than Prelude.String
Extract Inductive String.string => "Prelude.String" ["[]" "(:)"].
Extract Inlined Constant String.string_dec => "(Prelude.==)".*)

Extract Inductive nat => "Prelude.Integer" [ "0" "Prelude.succ" ] "(\fO fS n -> if n Prelude.< 0 then Prelude.error ""Internal Error: negative nat"" else if n Prelude.== 0 then fO () else fS (n Prelude.- 1))".

Extract Inductive positive => "Prelude.Integer" [
  "(\x -> 2 Prelude.* x Prelude.+ 1)"
  "(\x -> 2 Prelude.* x)"
  "1" ]
  "(\fI fO fH n -> if n Prelude.== 0 then Prelude.error ""Internal Error: non-positive positive"" else
                   if n Prelude.== 1 then fH () else
                   if Prelude.odd n
                   then fI (n `Prelude.div` 2)
                   else fO (n `Prelude.div` 2))".

Extract Inductive Z => "Prelude.Integer" [ "0" "(\x -> x)" "Prelude.negate" ]
  "(\fO fP fN n -> if n Prelude.== 0 then fO () else
                   if n Prelude.> 0 then fP n else
                   fN (Prelude.negate n))".

End commonExtractHaskell.

Module commonExtractOcaml (delay : DelayExtractionDefinitionToken).

Extract Inductive sigT => "(*)" [ "(,)" ].
Extract Inductive unit => unit [ "()" ].
Extract Inductive bool => bool [ true false ].
Extract Inductive sumbool => bool [ true false ].
Extract Inductive option => option [ Some None ].
Extract Inductive prod => "(*)" [ "(,)" ].
Extract Inductive list => list [ "[]" "(::)" ].

Extract Inductive Ascii.ascii => char [ ascii ]. (* extraction magically maps to native char *)
Extract Inlined Constant Ascii.ascii_dec => "(=)".

Extract Inductive nat => int [ "0" "(fun x -> x + 1)" ] "(fun fO fS n -> if n < 0 then failwith ""Internal Error: negative nat"" else if n = 0 then fO () else fS (n - 1))".

End commonExtractOcaml.

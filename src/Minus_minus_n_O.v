Lemma Minus_minus_n_O :
  forall n : nat, n = n - 0.
Proof.
  destruct n; reflexivity.
Qed.

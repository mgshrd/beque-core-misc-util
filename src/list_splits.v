Require Import List.

Require Import fold_left_extract_accu.
Require Import ltac_utils.

Definition reconstruct_split {A} (lp : list (list A)) :
  list A :=
  fold_left (fun e a => e ++ a) lp nil.

Inductive list_split {A} : list A -> list (list A) -> Prop :=
| ls_nil : list_split nil nil
| ls_cons : forall l' l ls, list_split l ls -> list_split (l' ++ l) (l'::ls).

Lemma list_split_correct :
  forall A (l : list A) ls,
    list_split l ls ->
    reconstruct_split ls = l.
Proof.
  intros. induction H.
  - unfold reconstruct_split. simpl. reflexivity.
  - unfold reconstruct_split in *. simpl.
    rewrite <- IHlist_split. clear.
    rewrite fold_left_extract_accu with (i:=nil); [ | exact (@app_nil_l _) | exact (@app_nil_r _) | exact (@app_assoc _) ].
    reflexivity.
Qed.

Lemma reconstruct_split_correct :
  forall A (l : list A) ls,
    reconstruct_split ls = l ->
    list_split l ls.
Proof.
  intros. subst l. induction ls.
  { unfold reconstruct_split. simpl. constructor. }
  unfold reconstruct_split. simpl.
  rewrite fold_left_extract_accu with (i:=nil); [ | exact (@app_nil_l _) | exact (@app_nil_r _) | exact (@app_assoc _) ].
  constructor.
  exact IHls.
Qed.

Lemma reconstruct_and_split_eq :
  forall A (l : list A) ls,
    list_split l ls <-> reconstruct_split ls = l.
Proof.
  intros. split.
  - apply list_split_correct.
  - apply reconstruct_split_correct.
Qed.

Lemma list_split_nil_cons :
  forall A (l : list A) lp,
    list_split l lp <-> list_split l (nil::lp).
Proof.
  intros. repeat rewrite reconstruct_and_split_eq. unfold reconstruct_split.
  split; intro.
  - simpl. exact H.
  - simpl in H. exact H.
Qed.

Fixpoint replace_first_with_segment
         {A}
         (eq_A_dec : forall a1 a2 : A, {a1=a2}+{a1<>a2})
         l a s :
  list A :=
  match l with
    | nil => nil
    | h::t => if eq_A_dec a h
              then s ++ t
              else h::(replace_first_with_segment eq_A_dec t a s)
  end.

Lemma replace_first_with_segment_not_in :
  forall A eq_A_dec l a (s : list A),
    ~In a l ->
    replace_first_with_segment eq_A_dec l a s = l.
Proof.
  intros ? ? ? ? ? nin.
  induction l.
  { simpl. reflexivity. }
  simpl. destruct eq_A_dec.
  - subst a0. elim nin. left. reflexivity.
  - apply f_equal2; [ reflexivity | ].
    apply IHl. clear IHl. intro.
    elim nin. right. exact H.
Qed.

Lemma replace_first_with_segment_nil :
  forall A eq_A_dec l a (s : list A),
    replace_first_with_segment eq_A_dec l a s = nil ->
    {In a l /\ s = nil /\ l = a::nil}+{~In a l /\ l = nil}.
Proof.
  intros.
  destruct (in_dec eq_A_dec a l).
  - left. split; [ exact i | ].
    destruct l; [ contradiction i | ].
    destruct i.
    + subst a0. simpl in H.
      destruct eq_A_dec; [ | discriminate H ].
      destruct (app_eq_nil _ _ H). subst.
      split; reflexivity.
    + simpl in H. destruct eq_A_dec.
      * subst a0.
        destruct (app_eq_nil _ _ H). subst.
        split; reflexivity.
      * discriminate H.
  - right.
    split; [ exact n | ].
    destruct l; [ reflexivity | ].
    simpl in H. destruct eq_A_dec.
    + elim n. left. subst a0. reflexivity.
    + discriminate H.
Qed.

Lemma list_split_head :
  forall A (l : list A) h ls,
    list_split l (h::ls) ->
    firstn (length h) l = h.
Proof.
  intros. remember (length h). revert l h ls H Heqn. induction n; intros.
  { destruct h; [ | discriminate Heqn ]. reflexivity. }
  destruct h; [ discriminate Heqn | ].
  simpl in *. injection Heqn. intros. clear Heqn.
  destruct l.
  { inversion H. subst. discriminate H1. }
  assert (Hx := list_split_correct _ _ _ H).
  unfold reconstruct_split in Hx. simpl in Hx.
  rewrite fold_left_extract_accu with (i:=nil) in Hx; [ | exact (@app_nil_l _) | exact (@app_nil_r _) | exact (@app_assoc _) ].
  simpl in Hx. injection Hx. intros. clear Hx. subst a0. subst l.
  apply f_equal2; [ reflexivity | ].
  apply IHn with (ls := ls); [ | exact H0 ].
  rewrite reconstruct_and_split_eq in *.
  unfold reconstruct_split in *. simpl in *.
  rewrite fold_left_extract_accu with (i:=nil); [ | exact (@app_nil_l _) | exact (@app_nil_r _) | exact (@app_assoc _) ].
  reflexivity.
Qed.

Lemma list_split_tail :
  forall A (l : list A) h ls,
    list_split l (h::ls) ->
    list_split (skipn (length h) l) ls.
Proof.
  intros. rewrite reconstruct_and_split_eq in *.
  unfold reconstruct_split in *. simpl in *. subst l.
  revert ls. induction h.
  - simpl. intros. reflexivity.
  - intros. simpl. rewrite IHh.
    destruct ls.
    { simpl. reflexivity. }
    simpl.
    magic_case (fold_left (fun e a0 : list A => e ++ a0) ls (a :: h ++ l)).
    { rewrite fold_left_extract_accu with (i:=nil) in H; [ | exact (@app_nil_l _) | exact (@app_nil_r _) | exact (@app_assoc _) ]. discriminate H. }
    rewrite fold_left_extract_accu with (i:=nil) in H; [ | exact (@app_nil_l _) | exact (@app_nil_r _) | exact (@app_assoc _) ].
    simpl in H. injit H. intros. subst.
    rewrite fold_left_extract_accu with (i:=nil); [ | exact (@app_nil_l _) | exact (@app_nil_r _) | exact (@app_assoc _) ].
    reflexivity.
Qed.

Lemma list_split_split :
  forall A (l : list A) h ls,
    list_split l (h::ls) ->
    firstn (length h) l = h /\ list_split (skipn (length h) l) ls.
Proof.
  intros. split.
  - apply list_split_head with ls. exact H.
  - apply list_split_tail. exact H.
Qed.

Lemma list_split_replace_head :
  forall A (l : list A) ls h hs,
    list_split l (h::ls) ->
    list_split h hs ->
    list_split l (hs ++ ls).
Proof.
  intros.
  destruct (list_split_split _ _ _ _ H).
  rewrite reconstruct_and_split_eq in *.
  unfold reconstruct_split in *. simpl in *.
  rewrite fold_left_extract_accu with (i:=nil) in H; [ | exact (@app_nil_l _) | exact (@app_nil_r _) | exact (@app_assoc _) ].
  rewrite <- H0 in H.
  rewrite fold_left_app.
  rewrite fold_left_extract_accu with (i:=nil); [ | exact (@app_nil_l _) | exact (@app_nil_r _) | exact (@app_assoc _) ].
  exact H.
Qed.

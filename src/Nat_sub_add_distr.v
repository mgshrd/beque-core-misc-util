Require Import Minus_minus_n_O.
Require Import Nat_sub_succ.

Require Import Arith.
Lemma Nat_sub_add_distr :
  forall n m p : nat, n - (m + p) = n - m - p.
Proof.
  intros. revert p n.
  induction m.
  - simpl. intros. rewrite <- Minus_minus_n_O. reflexivity.
  - simpl. intros.
    destruct n; [ solve [ auto ] | ].
    rewrite Nat_sub_succ. simpl. apply IHm.
Qed.

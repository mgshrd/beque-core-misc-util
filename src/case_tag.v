Ltac tuKLNxZxJL_move_to_top x :=
  match reverse goal with
    | H : _ |- _ => try move x after H
  end.

Inductive tuKLNxZxJL_subgoal_mark := tuKLNxZxJL_sgm.
Notation "'Case___'" := (tuKLNxZxJL_subgoal_mark)(at level 999, only printing).
Ltac SG a := match goal with
               | og : tuKLNxZxJL_subgoal_mark |- _ =>
                 let HH := fresh og "/" a in
                 rename og into HH
               | |- _ => let H := fresh a in
                         match goal with
                         | og : tuKLNxZxJL_subgoal_mark |- _ =>
                           fail 1 "internal error in SG"
                         | |- _ => idtac
                         end;
                         assert (H := tuKLNxZxJL_sgm); tuKLNxZxJL_move_to_top H
             end.

Tactic Notation "typeclass_method_marker" ident(A) ident(f) :=
  let id := fresh A "." f in
  SG id; refine _.


Tactic Notation "tuKLNxZxJL_assert_eq" ident(x) constr(v) :=
  let H := fresh in
  assert (x = v) as H by reflexivity;
    clear H.

Tactic Notation "tuKLNxZxJL_Case_aux" ident(x) constr(name) :=
  first [
      set (x := name); tuKLNxZxJL_move_to_top x
    | tuKLNxZxJL_assert_eq x name; tuKLNxZxJL_move_to_top x
    | fail 1 "because we are working on a different case" ].

Tactic Notation "Case" constr(name) := tuKLNxZxJL_Case_aux Case name.
Tactic Notation "SCase" constr(name) := tuKLNxZxJL_Case_aux SCase name.
Tactic Notation "SSCase" constr(name) := tuKLNxZxJL_Case_aux SSCase name.
Tactic Notation "SSSCase" constr(name) := tuKLNxZxJL_Case_aux SSSCase name.
Tactic Notation "SSSSCase" constr(name) := tuKLNxZxJL_Case_aux SSSSCase name.
Tactic Notation "SSSSSCase" constr(name) := tuKLNxZxJL_Case_aux SSSSSCase name.
Tactic Notation "SSSSSSCase" constr(name) := tuKLNxZxJL_Case_aux SSSSSSCase name.
Tactic Notation "SSSSSSSCase" constr(name) := tuKLNxZxJL_Case_aux SSSSSSSCase name.

(* importing List take ages *)

Remark List_app_assoc :
  forall A (l ll lll : list A),
    (l ++ ll ++ lll = (l ++ ll) ++ lll)%list.
Proof.
  induction l.
  - intros; reflexivity.
  - intros. simpl. rewrite IHl. reflexivity.
Qed.

Lemma Nat_add_comm :
  forall n m, n + m = m + n.
Proof.
  induction n; induction m; auto.
  simpl. rewrite IHn. simpl. rewrite <- IHm. simpl. rewrite IHn. reflexivity.
Qed.

Require Import List.
Require Import Omega.

Lemma head_eq_length :
  forall {A} (h : list A) hh t tt,
    h ++ t = hh ++ tt ->
    length h = length hh ->
    h = hh /\ t = tt.
Proof.
  induction h.
  - intros.
    destruct hh; [ | simpl in H0; omega ].
    split; [ reflexivity | ].
    simpl in H.
    exact H.
  - intros.
    destruct hh; [ discriminate H0 | ].
    injection H; clear H; intros; subst.
    simpl in H0. injection H0. intros.
    destruct (IHh _ _ _ H H1).
    subst.
    split; reflexivity.
Qed.

Lemma tail_eq_length :
  forall {A} (h : list A) hh t tt,
    h ++ t = hh ++ tt ->
    length t = length tt ->
    h = hh /\ t = tt.
Proof.
  intros.
  cut (length h = length hh).
  { intro. exact (head_eq_length _ _ _ _ H H1). }
  assert (length (h ++ t) = length (hh ++ tt)); [ rewrite H; reflexivity | ].
  repeat rewrite app_length in H1.
  rewrite H0 in *. omega.
Qed.

Lemma list_eq_append :
  forall {A} (l : list A) a ll,
    l = l ++ a::ll ->
    False.
Proof.
  intros.
  assert (length (l ++ a::ll) = length l).
  { rewrite <- H. reflexivity. }
  rewrite app_length in H0.
  simpl in H0.
  omega.
Qed.

Lemma list_eq_prepend :
  forall {A} (l : list A) a ll,
    ll = a::l ++ ll ->
    False.
Proof.
  intros.
  assert (length (a::l ++ ll) = length ll).
  { rewrite <- H. reflexivity. }
  rewrite app_comm_cons in H0. rewrite app_length in H0.
  simpl in H0.
  omega.
Qed.

Lemma list_eq_cons_app :
  forall {A} (l : list A) a ll,
    l = a::l ++ ll ->
    False.
Proof.
  intros.
  assert (length (a::l ++ ll) = length l).
  { rewrite <- H. reflexivity. }
  rewrite app_comm_cons in H0. rewrite app_length in H0.
  simpl in H0.
  omega.
Qed.

Lemma list_eq_app_cons :
  forall {A} (l : list A) a ll,
    ll = l ++ a::ll ->
    False.
Proof.
  intros.
  assert (length (l ++ a::ll) = length ll).
  { rewrite <- H. reflexivity. }
  rewrite app_length in H0.
  simpl in H0.
  omega.
Qed.

Lemma list_eq_cons_mid :
  forall {A} (ll : list A) a l lll,
    ll = (a::l) ++ ll ++ lll ->
    False.
Proof.
  intros.
  assert (length ((a::l) ++ ll ++ lll) = length ll).
  { rewrite <- H. reflexivity. }
  repeat rewrite app_length in H0.
  simpl in H0.
  omega.
Qed.

Lemma list_eq_mid_cons :
  forall {A} (ll : list A) a l lll,
    ll = l ++ ll ++ a::lll ->
    False.
Proof.
  intros.
  assert (length (l ++ ll ++ a::lll) = length ll).
  { rewrite <- H. reflexivity. }
  repeat rewrite app_length in H0.
  simpl in H0.
  omega.
Qed.
